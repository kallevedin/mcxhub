// Community for Tor and I2P. IRC and web server built for anonymity.
package main

import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"bitbucket.org/kallevedin/mcxhub/mcxircd"
	"bitbucket.org/kallevedin/mcxhub/mcxweb"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
)

// Starts the program
func main() {

	ncpu := runtime.NumCPU()
	runtime.GOMAXPROCS(ncpu + 1)

	// read flags
	ircPort := flag.Int("ircport", 6667, "What port the IRC server should listen at")
	webPort := flag.Int("webport", 6502, "What port the web server should listen at")
	conf := flag.String("conf", "./mcxhub.conf", "Config file to use.")
	motd := flag.String("motd", "./MOTD", "MOTD file to use.")
	opers := flag.String("opers", "./OPERS", "OPERS file")
	makepass := flag.String("makepass", "", "Creates passwords for the OPERS file, using the salt specified in your mcxhub config file.")
	flag.Parse()

	// read config files
	if s, ok := mcxconfig.ReadConfigFile(*conf); !ok {
		log.Printf("%s\n", s)
		log.Printf("Could not read config file %s, terminating.\n", *conf)
		os.Exit(1)
	}
	// optionally create a password for the OPERS file, then quit
	if *makepass != "" {
		hexpass := mcxconfig.MakeHashedAndSaltedPassword(*makepass)
		fmt.Printf("%s translates to:\n%s\n", *makepass, hexpass)
		os.Exit(0)
	}
	if s, ok := mcxconfig.ReadMOTDFile(*motd); !ok {
		// Non-fatal
		log.Printf("%s", s)
	}
	if s, ok := mcxconfig.ReadOPERSFile(*opers); !ok {
		// Non-fatal
		log.Printf("%s", s)
	}

	mcxconfig.SetIRCPort(*ircPort)
	mcxconfig.SetWebPort(*webPort)

	log.Printf("MCXHUB: starting services.\n")

	// maybe start MCXHUB services
	if mcxconfig.GetStartWebserver() {
		go mcxweb.StartWebServer()
	}
	go mcxircd.StartIRCServer()
//	go supportBot.Start("helpdesk", "#helpdesk")
	
	// It would be nice to have a goroutine that periodically checks if everything works, and if it doesnt
	// it tries to restart it. But for now, lets just wait forever.
	derp := make(chan bool)
	<-derp
	
}
