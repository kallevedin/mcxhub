package mcxircd





import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"bufio"
	"regexp"
	"strings"
	"time"
)





var (
	validUsername       string         = "[a-zA-Z\\[\\]\\\\`_\\^\\{\\}][a-zA-Z0-9\\-\\[\\]\\\\`_\\^\\{\\}]*"
	validUsernameRegexp *regexp.Regexp = regexp.MustCompile("^" + validUsername + "$")
)





// Returns false if the nickname is not valid. Does NOT check if the nickname is taken!
func validNickname(nickname string) (bool) {
	if nickname == "" {
		return false
	}
	if len(nickname) > mcxconfig.GetMaxNickLen() {
		return false
	}
	if !validUsernameRegexp.MatchString(nickname) {
		return false
	}
	return true
}



// Returns true if the nickname is protected - one should never be able to switch to these nicknames.
func protectedNickname(nickname string) (bool) {
	if nickname == "anonymous" || nickname == "SERVER" {
		return true
	}
	return false
}



// Enables or disables spam protection for this user. Useful for bots that are
// known to be friendly, and need to handle a lot of users!
func (myself *IRCUser) SetSpamProtection(enable bool) {
	if myself.RefAttach() {
		myself.setSpamProtection <- enable
		myself.RefDeattach()
	}
}



// Adds spam penalty to the user in the form of milliseconds until next command
// will be interpreted.
func (myself *IRCUser) addSpamPenalty(milliseconds int64) {
	if myself.spamProtectionEnabled {
		penalty := time.Duration(milliseconds)
		a := myself.spamPenalty.Add(penalty * time.Millisecond)
		b := time.Now().Add(penalty * time.Millisecond)	
		if a.After(b) {
			myself.spamPenalty = a
		} else {
			myself.spamPenalty = b
		}
	}
}



// Will cause the IRCUser's command parser-goroutine to sleep for a short while
// (that is, not handle incomming commands from the IRCUser) if the user has 
// been sending too many messages lately.
func (myself *IRCUser) spamProtectionSleep() {
	if myself.spamProtectionEnabled {
		time.Sleep(myself.spamPenalty.Sub(time.Now().Add(10 * time.Second)))
	}
}



// updates the users mode according to the modeCommand. See RFC2812 section
// 3.1.5 for more information. this replies with the message that results from
// applying a MODE command on a user.
func (myself *IRCUser) updateMode(user *IRCUser, modeCommand string) string {
	IsOperator := myself.IsOperator
	Away := myself.Away
	Invisible := myself.Invisible
	Wallops := myself.Wallops
	Restricted := myself.Restricted
	ModeS := myself.ModeS
	mod := true

	words := bufio.NewScanner(strings.NewReader(modeCommand))
	words.Split(bufio.ScanWords)
	for words.Scan() {
		s := bufio.NewScanner(strings.NewReader(words.Text()))
		s.Split(bufio.ScanRunes)
		for s.Scan() {
			r := rune(s.Text()[0])
			switch r {
			case '+':
				mod = true
			case '-':
				mod = false
			case 'a':
				Away = mod
			case 'i':
				Invisible = mod
			case 'w':
				Wallops = mod
			case 's':
				ModeS = mod
			case 'o':
				if user.IsOperator {
					IsOperator = mod
				} else {
					return ERR_NOPRIVILEGES(user.Nickname)
				}
			default:
				return ERR_UMODEUNKNOWNFLAG(user.Nickname)
			}
		}
		if err := s.Err(); err != nil {
			return ERR_NEEDMOREPARAMS(user.Nickname, "MODE")
		}
	}
	if err := words.Err(); err != nil {
		return ERR_NEEDMOREPARAMS(user.Nickname, "MODE")
	}
	myself.IsOperator = IsOperator
	myself.Away = Away
	myself.Invisible = Invisible
	myself.Wallops = Wallops
	myself.Restricted = Restricted
	myself.ModeS = ModeS
	return RPL_UMODEIS(myself.Nickname, myself.modeString())
}



// Returns the mode string of the user
func (myself *IRCUser) modeString() string {
	str := "+"
	if myself.Away {
		str += "a"
	}
	if myself.Invisible {
		str += "i"
	}
	if myself.Wallops {
		str += "w"
	}
	if myself.IsOperator {
		str += "o"
	}
	if myself.Restricted {
		str += "r"
	}
	if myself.ModeS {
		str += "s"
	}
	return str
}




