package mcxircd

import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"fmt"
	"strings"
)

func RPL_WELCOME(nick string) string {
	return fmt.Sprintf(":%s 001 %s Welcome to the Internet Relay Network %s!%s@%s\r\n", mcxconfig.GetFullName(), nick, nick, nick, mcxconfig.GetHostcloak())
}

func RPL_YOURHOST(nick string) string {
	return fmt.Sprintf(":%s 002 %s Your host is %s, running version %s\r\n", mcxconfig.GetFullName(), nick, mcxconfig.GetFullName(), mcxconfig.GetVersion())
}

func RPL_CREATED(nick string) string {
	return fmt.Sprintf(":%s 003 %s Om! Hare Krsna Hare Krsna - Krsna Krsna Hare Hare - Hare Rama Hare Rama - Rama Rama Hare Hare\r\n", mcxconfig.GetFullName(), nick)
}

func RPL_MYINFO(nick string) string {
	return fmt.Sprintf(":%s 004 %s %s %s aiowrs Aknot\r\n", mcxconfig.GetFullName(), nick, mcxconfig.GetShortName(), mcxconfig.GetVersion())
}

func RPL_UMODEIS(nick, umode string) string {
	return fmt.Sprintf(":%s 221 %s %s\r\n", mcxconfig.GetFullName(), nick, umode)
}

func RPL_SIMPLEWHOISUSER(nick, target string) string {
	return RPL_WHOISUSER(nick, target, target, mcxconfig.GetHostcloak(), mcxconfig.GetDefaultRealName())
}

func RPL_UNAWAY(nick string) string {
	return fmt.Sprintf(":%s 305 %s :You are no longer marked as being away\r\n", mcxconfig.GetHostcloak(), nick)
}

func RPL_NOWAWAY(nick string) string {
	return fmt.Sprintf(":%s 306 %s :You have been marked as being away\r\n", mcxconfig.GetHostcloak(), nick)
}

func RPL_WHOISUSER(nick, target, user, host, realName string) string {
	return fmt.Sprintf(":%s 311 %s %s %s %s * :%s\r\n", mcxconfig.GetFullName(), nick, target, user, host, realName)
}

func RPL_ENDOFWHO(nick, itemName string) string {
	return fmt.Sprintf(":%s 315 %s %s :End of WHO list\r\n", mcxconfig.GetFullName(), nick, itemName)
}

func RPL_ENDOFWHOIS(nick, target string) string {
	return fmt.Sprintf(":%s 318 %s %s :End of WHOIS list\r\n", mcxconfig.GetFullName(), nick, target)
}

func RPL_LIST(nick, channel, topic string, numVisible int) string {
	return fmt.Sprintf(":%s 322 %s %s %d :%s\r\n", mcxconfig.GetFullName(), nick, channel, numVisible, topic)
}

func RPL_LISTEND(nick string) string {
	return fmt.Sprintf(":%s 323 %s :End of LIST\r\n", mcxconfig.GetFullName(), nick)
}

func RPL_CHANNELMODEIS(nick, channel, modeString string) string {
	return fmt.Sprintf(":%s 324 %s %s %s\r\n", mcxconfig.GetFullName(), nick, channel, modeString)
}

func RPL_NOTOPIC(nick, channel string) string {
	return fmt.Sprintf(":%s 331 %s %s :No topic is set\r\n", mcxconfig.GetFullName(), nick, channel)
}

func RPL_TOPIC(nick, channel, topic string) string {
	return fmt.Sprintf(":%s 332 %s %s :%s\r\n", mcxconfig.GetFullName(), nick, channel, topic)
}

func RPL_MAYBETOPIC(nick, channel, topic string) string {
	if topic == "" {
		return RPL_NOTOPIC(nick, channel)
	}
	return RPL_TOPIC(nick, channel, topic)
}

func RPL_INVITELIST(nick, channel, inviteMask string) string {
	return fmt.Sprintf(":%s 346 %s %s %s\r\n", mcxconfig.GetFullName(), nick, channel, inviteMask)
}

func RPL_ENDOFINVITELIST(nick, channel string) string {
	return fmt.Sprintf(":%s 347 %s %s :End of channel invite list\r\n", mcxconfig.GetFullName(), nick, channel)
}

func RPL_EXCEPTLIST(nick, channel, exceptionMask string) string {
	return fmt.Sprintf(":%s 348 %s %s %s\r\n", mcxconfig.GetFullName(), nick, channel, exceptionMask)
}

func RPL_ENDOFEXCEPTLIST(nick, channel string) string {
	return fmt.Sprintf(":%s 349 %s %s\r\n", mcxconfig.GetFullName(), nick, channel)
}

func RPL_VERSION(nick string) string {
	return fmt.Sprintf(":%s 351 %s %s.0 %s :%s\r\n", mcxconfig.GetFullName(), nick, mcxconfig.GetVersion(), mcxconfig.GetShortName(), mcxconfig.GetQuote())
}

func RPL_SIMPLEWHOREPLY(nick, channel, target string, hasOp bool) string {
	weirdsymbols := "H"
	if hasOp {
		weirdsymbols = "H@"
	}
	return RPL_WHOREPLY(nick, channel, target, mcxconfig.GetHostcloak(), mcxconfig.GetFullName(), target, weirdsymbols, mcxconfig.GetDefaultRealName(), 0)
}

func RPL_WHOREPLY(nick, channel, user, host, usersServer, target, weirdsymbols, realName string, hopcount int) string {
	// It is unknown what the "wierdsymbols" mean. See RFC2812 page 46.
	return fmt.Sprintf(":%s 352 %s %s %s %s %s %s %s :%d %s\r\n", mcxconfig.GetFullName(), nick, channel, user, host, usersServer, target, weirdsymbols, hopcount, realName)
}

func RPL_NAMREPLY(nick, channel string, nicknames []string) string {
	typeOfChannel := "=" // normal chan
	if channel == "*" {
		typeOfChannel = "*"
	}
	result := fmt.Sprintf(":%s 353 %s %s %s :", mcxconfig.GetFullName(), nick, typeOfChannel, channel)
	for _, name := range nicknames {
		result += name + " "
	}
	strings.TrimSpace(result)
	result += "\r\n"
	return result
}

func RPL_ENDOFNAMES(nick, channel string) string {
	return fmt.Sprintf(":%s 366 %s %s :End of NAMES list\r\n", mcxconfig.GetFullName(), nick, channel)
}

func RPL_BANLIST(nick, channel, banMask string) string {
	return fmt.Sprintf(":%s 367 %s %s %s\r\n", mcxconfig.GetFullName(), nick, channel, banMask)
}

func RPL_ENDOFBANLIST(nick, channel string) string {
	return fmt.Sprintf(":%s 368 %s %s :End of channel ban list\r\n", mcxconfig.GetFullName(), nick, channel)
}

func RPL_INFO(nick string) string {
	return fmt.Sprintf(":%s 371 %s :software: %s, version: %s\r\n", mcxconfig.GetFullName(), nick, mcxconfig.GetSoftware(), mcxconfig.GetVersion())
}

func RPL_MOTD(nick string) string {
	motdStr := ""
	if mcxconfig.MOTD == nil {
		for i := 0; i != 10; i++ {
			motdStr += fmt.Sprintf(":%s 372 %s :- \x02\x034Om mani padme hum\x03\x02\r\n", mcxconfig.GetFullName(), nick)
		}
	} else {
		for i := 0; i != len(mcxconfig.MOTD); i++ {
			motdStr += fmt.Sprintf(":%s 372 %s :- %s\r\n", mcxconfig.GetFullName(), nick, string(mcxconfig.MOTD[i]))
		}
	}
	return motdStr
}

func RPL_ENDOFINFO(nick string) string {
	return fmt.Sprintf(":%s 374 %s :End of INFO list\r\n", mcxconfig.GetFullName(), nick)
}

func RPL_MOTDSTART(nick string) string {
	return fmt.Sprintf(":%s 375 %s :- %s Message of the day - \r\n", mcxconfig.GetFullName(), nick, mcxconfig.GetShortName())
}

func RPL_ENDOFMOTD(nick string) string {
	return fmt.Sprintf(":%s 376 %s :End of MOTD command\r\n", mcxconfig.GetFullName(), nick)
}

func RPL_YOUREOPER(nick string) string {
	return fmt.Sprintf(":%s 381 %s :You are now an IRC operator\r\n", mcxconfig.GetFullName(), nick)
}

func RPL_REHASHING(nick string) string {
	return fmt.Sprintf(":%s 382 %s ALLFILES :Rehashing\r\n", mcxconfig.GetFullName(), nick)
}

func PRIVMSG(from, to, message string) string {
	return fmt.Sprintf(":%s!%s@%s PRIVMSG %s :%s\r\n", from, from, mcxconfig.GetHostcloak(), to, message)
}

func JOIN(nick, channel string) string {
	return fmt.Sprintf(":%s!%s@%s JOIN %s\r\n", nick, nick, mcxconfig.GetHostcloak(), channel)
}

func KICK(nick, douchebag, channel, reason string) string {
	return fmt.Sprintf(":%s!%s@%s KICK %s %s :%s\r\n", nick, nick, mcxconfig.GetHostcloak(), channel, douchebag, reason)
}

func NICK(oldNick, newNick string) string {
	return fmt.Sprintf(":%s!%s@%s NICK %s\r\n", oldNick, oldNick, mcxconfig.GetHostcloak(), newNick)
}

func MODE(nick, target, modeString string) string {
	return fmt.Sprintf(":%s!%s@%s MODE %s %s\r\n", nick, nick, mcxconfig.GetHostcloak(), target, modeString)
}

func PART(nick, channel string) string {
	return fmt.Sprintf(":%s!%s@%s PART %s :\r\n", nick, nick, mcxconfig.GetHostcloak(), channel)
}

func TOPIC(nick, channel, topic string) string {
	return fmt.Sprintf(":%s!%s@%s TOPIC %s :%s\r\n", nick, nick, mcxconfig.GetHostcloak(), channel, topic)
}

func ERR_NOSUCHNICK(nick, nonExistant string) string {
	return fmt.Sprintf(":%s 401 %s %s :No such nick/channel\r\n", mcxconfig.GetFullName(), nick, nonExistant)
}

func ERR_NOSUCHSERVER(nick, nonExistant string) string {
	return fmt.Sprintf(":%s 402 %s %s :No such server\r\n", mcxconfig.GetFullName(), nick, nonExistant)
}

func ERR_NOSUCHCHANNEL(nick, channel string) string {
	return fmt.Sprintf(":%s 403 %s %s :No such channel\r\n", mcxconfig.GetFullName(), nick, channel)
}

func ERR_WASNOSUCHNICK(nick, target string) string {
	return fmt.Sprintf(":%s 406 %s %s :There was no such nickname\r\n", mcxconfig.GetFullName(), nick, target)
}

func ERR_TOOMANYTARGETS(nick, someClue string) string {
	return fmt.Sprintf(":%s 407 %s %s :Too many targets.\r\n", mcxconfig.GetFullName(), nick, someClue)
}

func ERR_ERRONEUSNICKNAME(nick, badNick string) string {
	return fmt.Sprintf(":%s 432 %s %s :Erroneous nickname\r\n", mcxconfig.GetFullName(), nick, badNick)
}

func ERR_NICKNAMEINUSE(currentNick, desiredNick string) string {
	return fmt.Sprintf(":%s 433 %s %s :Nickname is already in use\r\n", mcxconfig.GetFullName(), currentNick, desiredNick)
}

func ERR_NOTONCHANNEL(nick, channel string) string {
	return fmt.Sprintf(":%s 442 %s %s :You're not on that channel\r\n", mcxconfig.GetFullName(), nick, channel)
}

func ERR_NICKCOLLISION(nick string) string {
	return fmt.Sprintf(":%s 436 %s :Nickname collision KILL from %s@%s", mcxconfig.GetFullName(), nick, nick, mcxconfig.GetHostcloak())
}

func ERR_USERNOTINCHANNEL(nick, notPresent, channel string) string {
	return fmt.Sprintf("%s 441 %s %s %s :They aren't on that channel\r\n", mcxconfig.GetFullName(), nick, notPresent, channel)
}

func ERR_NEEDMOREPARAMS(nick, command string) string {
	return fmt.Sprintf(":%s 461 %s %s :Not enough parameters\r\n", mcxconfig.GetFullName(), nick, command)
}

func ERR_PASSWDMISMATCH(nick string) string {
	return fmt.Sprintf(":%s 464 %s :Password incorrect\r\n", mcxconfig.GetFullName(), nick)
}

func ERR_UNKNOWNMODE(nick, channel string, badMode string) string {
	return fmt.Sprintf(":%s 472 %s %s :is unknown mode char to me for %s\r\n", mcxconfig.GetFullName(), nick, badMode, channel)
}

func ERR_BANNEDFROMCHAN(nick, channel string) string {
	return fmt.Sprintf(":%s 474 %s %s :Cannot join channel (+b)\r\n", mcxconfig.GetFullName(), nick, channel)
}

func ERR_BADCHANNELKEY(nick, channel string) string {
	return fmt.Sprintf(":%s 475 %s %s :Cannot join channel (+k)\r\n", mcxconfig.GetFullName(), nick, channel)
}

func ERR_NOPRIVILEGES(nick string) string {
	return fmt.Sprintf(":%s 481 %s :Permission Denied- You're not an IRC operator\r\n", mcxconfig.GetFullName(), nick)
}

func ERR_CHANOPRIVSNEEDED(nick, channel string) string {
	return fmt.Sprintf(":%s 482 %s %s :You're not channel operator\r\n", mcxconfig.GetFullName(), nick, channel)
}

func ERR_UMODEUNKNOWNFLAG(nick string) string {
	return fmt.Sprintf(":%s 501 %s :Unknown MODE flag\r\n", mcxconfig.GetFullName(), nick)
}

func ERR_USERSDONTMATCH(nick string) string {
	return fmt.Sprintf(":%s 502 %s :Cannot change mode for other users\r\n", mcxconfig.GetFullName(), nick)
}

