package mcxircd

import (
	"testing"
)

func TestbadCTCP(t *testing.T) {
	if badCTCP(":hello!hello@server.com QWHATEVER meh :\x01VERSION\x01") {
		t.Fail()
	}
	if !badCTCP(":hello!hello@server.com PRIVMSG meh :\x01VERSION\x01") {
		t.Fail()
	}
	if badCTCP(":hello!hello@server.com PRIVMSG meh :\x01ACTION\x01") {
		t.Fail()
	}
	if badCTCP(":meh") {
		t.Fail()
	}
	if !badCTCP("PRIVMSG meh :\x01VERSION\x01") {
		t.Fail()
	}
	if badCTCP("PRIVMSG #hello :\x02ACTION") {
		t.Fail()
	}
	if !badCTCP("NOTICE meh :\x01VERSION") {
		t.Fail()
	}
	if !badCTCP("PRIVMSG +hello :\x01ATTACK") {
		t.Fail()
	}
	if badCTCP(":nick!user@server.somewhere.com PRIVMSG :") {
		t.Fail()
	}
}
