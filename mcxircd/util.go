package mcxircd

import (
	"bufio"
	"strings"
	"regexp"
)




type ScanWordsOrCUTObj struct {
	data []byte
}

func makeScanWordsOrCUT() *ScanWordsOrCUTObj {
	return &ScanWordsOrCUTObj{data: make([]byte, 0)}
}

func (s *ScanWordsOrCUTObj) ScanWordsOrCUT(data []byte, atEOF bool) (advance int, token []byte, err error) {
	advance, token, err = bufio.ScanWords(data, atEOF)
	s.data = data
	return advance, token, err
}

func (s *ScanWordsOrCUTObj) CUT() []byte {
	return s.data
}



// returns the first word in a sentence (actually the first sequence of bytes that ends with a space)
func firstWord(sentence string) string {
	for i := 0; i != len(sentence); i++ {
		if sentence[i] == ' ' {
			return sentence[:i]
		}
	}
	return sentence
}



// same as firstWord, except that it also splits at ANYTHING that is not a letter or number
func firstSequenceOfLetters(sentence string) string {
	for i := 0; i != len(sentence); i++ {
		if sentence[i] == ' ' || sentence[i] == '\x01' { // TODO REPLACE WITH !isletter() || !isnumber() etc
			return sentence[:i]
		}
	}
	return sentence
}



var privmsgORnotice *regexp.Regexp = regexp.MustCompile(`^(\S* )? *PRIVMSG|NOTICE|SQUERY \S* :`)

// returns true if the IRC command is a sensetive CTCP message. such messages
// can be used to reveal unnecessarily much about the users identity.
func badCTCP(message string) bool {
	if len(message) == 0 {
		return false
	}
	if privmsgORnotice.MatchString(message) {
		splitStrs := strings.SplitN(message, ":", 3)
		var msg string
		if len(splitStrs) == 3 && message[0] == ':' {
			msg = splitStrs[2]
		} else if len(splitStrs) == 2 && len(splitStrs[0]) > 0 && message[0] != ':' {
			msg = splitStrs[1]
		} else {
			return false
		}
		if len(msg) > 0 && msg[0] == '\x01' {
			// its a CTCP message
			if strings.EqualFold(firstSequenceOfLetters(msg[1:]), "ACTION") {
				// it is a "/me" CTCP command, which is OK.
				return false
			}
			// its a CTCP that is not a /me (ACTION)
			return true
		}
	}
	return false
}

