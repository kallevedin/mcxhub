package mcxircd

/*

IRCUser consists of three things: An IRCUser struct, and two goroutines. One
goroutine for message passing and sending messages to the users socket, located
in ircUserMessenger(), and one for parsing incomming messages from the users
socket, located in ircUserLoop().

ircUserMessenger() -- sends/receives on channels, writes to the socket.
ircUserLoop() -- an IRC command parser that is almost always blocked on reading.

To create a new IRC user, use NewIRCUser(..). Notice that you can use anything
that is a net.Conn for the connection (UNIX sockets, pipes, TCP, websockets, or
whatever.)



When sending messages to an IRCUser over channels: You need to make sure that 
the IRC user does not die before receiving on the channel, and optionally also
that it replies. At any moment, the user connected to the IRCUser socket can 
issue a DIE/QUIT/BYE-command, which terminates the IRCUser. Or it may die from
other causes. You need to tell the IRCUser not to die before it has processed 
your message!

You do this by calling RefAttach(). When you are done with sending messages with
the IRCUser, you call RefDeattach(). Like so:

if ircUser.RefAttach() {
	ircUser.someChannel <- someMessage
	...
	ircUser.RefDeattach()
} else {
	// user is in DYING mode. You SHOULD NOT send messages to it.
}



The IRCUser is a fuzzy two-state machine: RUNNING, and DYING. When it is RUNNING
it behaves as normal. When it receives a Kill-signal through its Kill-channel,
or when its socket is closed, it goes into DYING mode. In DYING mode, it first 
removes all references to itself from global datastructures (disconnects from 
channels and unregisters its nickname). Then it waits for everyone to finish
sending their messages to it, and when the last RefDeattach() has been called,
it kills itself. (And then the garbage collector mops up the mess.)

More detailed:
 RUNNING:
 0) Everything is normal
 1) Kill-message or socket close/error
 2) ircUserLoop() calls cleanDestroy()
 DYING:
 3) cleanDestroy() removes all references to the IRCUser from all global 
    datastructures
 4) cleanDestroy() sends a die-message to the ircUserMessenger()-goroutine
 5) cleanDestroy() returns and the ircUserLoop()-goroutine dies
 6) ircUserMessenger() close the socket, if that has not already happened, then
    process messages as normal, and waits for everyone to call RefDeattach()
 7) When everyone is done, ircUserMessenger dies
 8) nirvana

*/



import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"bufio"
//	"bytes"
//	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"sync"
	"time"
)



//Represents an IRC user. This struct contains values that are hidden. You should never write to this structure.
type IRCUser struct {
	// The Nickname is the username is the pseudonym of the user
	Nickname				string
	
	// Connection that is associated with the user.
	Conn					net.Conn

	// Sending True to this will violently but cleanly disconnect the user
	Kill					chan (bool)

	// Waitgroup that will block until this IRC user has disconnected or in any
	// way stopped existing.
	WaitForDeath			sync.WaitGroup

	// user flags
	IsOperator		bool	// The only flag that is used by mcxircd! All other are ignored.
	Away			bool
	Invisible		bool
	Wallops			bool
	Restricted		bool
	ModeS			bool

	// These are used internally only.
	
	waitingForPong			bool					// true if we have sent a PING to the user, and is waiting for it to reply with a PONG.
	spamProtectionEnabled	bool					// true, if spam protection is enabled for this user. use setSpamProtection/SetSpamProtection().
	timeout					time.Time				// Used to detect inactive/dead clients (time until next ping -> time to wait for pong -> KILL)
	spamPenalty				time.Time				// How long to wait until next command is parsed.
	pong					chan (bool)				// used to signal incomming PONG
	updateTimeout			chan (bool)				// used to signal that the user did something to show it is still alive. Updates the timeout.
	rawOutput				chan ([]byte)			// Anything sent to this channel will be written to the users socket.
	setMode					chan (*UserModeUpdate)	// Change mode flags of the user.
	setSpamProtection		chan (bool)				// Sets or disables spam protection.
	die						chan (bool)				// used when the goroutine

	// used internally for RefAttach/RefDeattach
	referenceCounterMutex	sync.RWMutex
	referenceCounter		int
}



// Can be sent to IRCUser's when others than themselves change their modes. (OPERS or bots for example)
type UserModeUpdate struct {
	User        *IRCUser
	ModeCommand string
	ReplyTo     chan (string)
}




// Creates a new IRCUser. Everything written to the net.Conn will be parsed as
// user IRC commands and replies will be sent back. Returns the datastruct and
// true if successful.
func NewIRCUser(conn net.Conn, nickname string) (*IRCUser, bool) {
	if !validNickname(nickname) || protectedNickname(nickname) {
		return nil, false
	}
	myself := IRCUser{}
	myself.Nickname = nickname
	myself.Conn = conn
	myself.waitingForPong = false
	myself.IsOperator = false
	myself.Invisible = true
	myself.spamProtectionEnabled = true
	myself.timeout = time.Now().Add(TIMEOUT_PING * time.Second)

	myself.pong = make(chan bool)
	myself.rawOutput = make(chan []byte)
	myself.updateTimeout = make(chan bool)
	myself.setMode = make(chan *UserModeUpdate)
	myself.setSpamProtection = make(chan bool)
	myself.die = make(chan bool)
	myself.Kill = make(chan bool)

	myself.referenceCounter = 1

	nameOK := make(chan bool)
	NameSpace.Add <- &NSAddRequest{Key: nickname, Value: &myself, ReplyTo: nameOK}
	ok := <-nameOK
	if ok {
		usersInChannelsMutex.Lock()
		usersInChannels[nickname] = make(map[string]*ircChannel)
		usersInChannelsMutex.Unlock()

		myself.WaitForDeath.Add(1) // cleared when the goroutine in ircUserMessenger() dies

		go myself.ircUserLoop()
		go myself.ircUserMessenger()
	}
	return &myself, ok
}



// Executes as a goroutine, and is very tightly coupled with ircUserLoop and IRCUser. This handles
// most message passing with other users and channels (that is, "chatting"). It also handles sending
// PINGs, if the user has not said anything for a while.
func (myself *IRCUser) ircUserMessenger() {
	defer myself.WaitForDeath.Done()
	var deathTicker (<- chan time.Time)

	for {
		timeoutIntervall := myself.timeout.Sub(time.Now())
		select {

		// this channel is created while we process the last messages sent to the IRCUser, and when the
		// last message has been sent, and processed, we die.
		case <-deathTicker :
			if myself.referenceCounter == 0 {
				return
			}
 
		// user replies with PONG to a PING message
		case <-myself.pong :
			myself.timeout = time.Now().Add(TIMEOUT_PING * time.Second)
			myself.waitingForPong = false

		// user does anything at all, and thus shows that it is alive
		case <-myself.updateTimeout :
			if !myself.waitingForPong {
				myself.timeout = time.Now().Add(TIMEOUT_PING * time.Second)
			}

		// something (such as an operator) made a mode change on this user
		case req := <-myself.setMode :
			if req.ModeCommand == "GET" {
				req.ReplyTo <- myself.modeString()
			}
			req.ReplyTo <- myself.updateMode(req.User, req.ModeCommand)

		// something enables/disables spam protection for this user (useful for known friendly and well-behaving bots)
		case b := <-myself.setSpamProtection :
			myself.spamProtectionEnabled = b

		// ANYTHING that comes in to rawOutput is dumped as it is at the user, unless its a CTCP command.
		case rawbytes := <-myself.rawOutput :
			if !badCTCP(string(rawbytes)) {
				myself.Conn.Write(rawbytes)
			}

		// If the user is inactive for too much time, this happens (we send PING to the user)
		case <-time.After(timeoutIntervall) :
			if myself.waitingForPong {
				myself.Conn.Write([]byte("ERROR :PING TIMEOUT\r\n"))
				myself.Conn.Close()
			} else {
				myself.waitingForPong = true
				myself.Conn.Write([]byte("PING :Gauranga!\r\n"))
				myself.timeout = time.Now().Add(TIMEOUT_PONG * time.Second)
			}

		// used to kill this goroutine. Will begin processing the last messages.
		case <-myself.die :
			myself.RefDeattach()
			myself.Conn.Close()
			deathTicker = time.Tick(1 * time.Second) // start the ticker and wait for everyone to send their final goodbyes

		// Kills the other goroutine, and waits for it to kill this goroutine 
		// (using a die-message).
		case <-myself.Kill :
			myself.Conn.Close()

		}
	}
}



// Executes as a goroutine. It says "HELLO!" to the user and then goes into an
// (almost) eternal loop, where it parses user input from the associated socket,
// and tries to be as RFC2812-compliant (RTFM) as possible.
func (myself *IRCUser) ircUserLoop() {
	defer myself.cleanDestroy()

	// users are born with spam penalty, to make it uneconomical to reconnect
	// as soon as anti-spam starts kicking in
	myself.addSpamPenalty(10000)

	// hello and welcome!
	myself.rawOutput <- []byte(
		RPL_WELCOME(myself.Nickname) +
			RPL_YOURHOST(myself.Nickname) +
			RPL_CREATED(myself.Nickname) +
			RPL_MYINFO(myself.Nickname) +
			RPL_MOTDSTART(myself.Nickname) +
			RPL_MOTD(myself.Nickname) +
			RPL_ENDOFMOTD(myself.Nickname) +
			RPL_UMODEIS(myself.Nickname, myself.modeString()))

	// a reader that reads at most 1024 bytes, and cuts user input as expected by an IRC server ~:)
	reader := bufio.NewReaderSize(myself.Conn, 1024)
	for {
		bline, isPrefix, err := reader.ReadLine()
		if err != nil {
			myself.Conn.Write([]byte("ERROR: " + err.Error() + "\r\n"))
			return
		}
		line := string(bline)
		if isPrefix {
			line += "[CUT]"
		}
		for i:=0; isPrefix; i++ {
			_, isPrefix, err = reader.ReadLine()
			if err != nil {
				return
			}
			if i >= 10 {
				return
			}
		}
		myself.spamProtectionSleep()
		
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		cutter := makeScanWordsOrCUT()
		wordScanner.Split(cutter.ScanWordsOrCUT)
		if !wordScanner.Scan() {
			continue
		}
		command := wordScanner.Text()

		myself.updateTimeout <- true

		/*
			 ____   ____   ____      _____   ____  ______
			|    \ |    | /    |    |     | /    ||      |
			|  o  ) |  | |   __|    |   __||  o  ||      |
			|     | |  | |  |  |    |  |_  |     ||_|  |_|
			|  O  | |  | |  |_ |    |   _] |  _  |  |  |
			|     | |  | |     |    |  |   |  |  |  |  |
			|_____||____||___,_|    |__|   |__|__|  |__|

			  _____ __    __  ____  ______   __  __ __
			 / ___/|  |__|  ||    ||      | /  ]|  |  |
			(   \_ |  |  |  | |  | |      |/  / |  |  |
			 \__  ||  |  |  | |  | |_|  |_/  /  |  _  |
			 /  \ ||  `  '  | |  |   |  |/   \_ |  |  |
			 \    | \      /  |  |   |  |\     ||  |  |
			  \___|  \_/\_/  |____|  |__| \____||__|__|

			  ...that interprets incomming commands from connected users such
			  that whatever makes IRC tick, tick.
			  Essentially, this switch is an implementation of RFC2812.
			  https://www.rfc-editor.org/rfc/rfc2812.txt
		*/

		switch strings.ToUpper(command) {

		case "PING":
			myself.rawOutput <- []byte("PONG" + line[4:] + "\r\n")

		case "PONG":
			myself.pong <- true

		case "QUIT", "BYE", "DIE":
			myself.rawOutput <- []byte("ERROR : <3 <3 <3 <3 <3 <3 <3 <3 <4 <5 <6 <7 <8 <9 <0 <+ <+ <+\r\n")
			return

		case "HELP":
			myself.addSpamPenalty(1000)
			myself.rawOutput <- []byte("INFO : Cheat sheet for terminal junkies\r\n" +
				"INFO : \r\n" +
				"INFO : change nickname:               \"NICK newnick\"\r\n" +
				"INFO : join channel:                  \"JOIN #channel\" or \"JOIN #channel password\" if password-protected\r\n" +
				"INFO : leave channel:                 \"PART #channel\"\r\n" +
				"INFO : list users in channel:         \"NAMES #channel\"\r\n" + 
				"INFO : list visible channels:         \"LIST\"\r\n" +
				"INFO : send message to channel:       \"PRIVMSG #channel :your message\"\r\n" +
				"INFO : send message to other user:    \"PRIVMSG nickname :your message\"\r\n" +
				"INFO : change channel mode:           \"MODE #chan +/-modeflags\" (example: MODE #derp +At)\r\n" +
				"INFO : op/deop:                       \"MODE #chan +/-o username\"\r\n" +
				"INFO : kick user:                     \"KICK #chan nickname go away!\"\r\n" +
				"INFO : set channel password:          \"MODE #chan +k single-sequence-password\"\r\n" +
				"INFO : get channel topic:             \"TOPIC #chan\"\r\n" +
				"INFO : set channel topic:             \"TOPIC #chan :new topic\"\r\n" +
				"INFO : gain system operator status:   \"OPER nickname single-sequence-password\"\r\n" +
				"INFO : kill user:                     \"KILL nickname\"\r\n" +
				"INFO : make server close your stream: \"DIE\" or \"QUIT\" or \"BYE\"\r\n" +
				"INFO : \r\n" +
				"INFO : Read RFC2812 (http://tools.ietf.org/html/rfc2812) for more!\r\n");
			continue

		case "AWAY":
			if wordScanner.Scan() {
				myself.rawOutput <- []byte(myself.updateMode(myself, "+a") + RPL_NOWAWAY(myself.Nickname))
			} else {
				myself.rawOutput <- []byte(myself.updateMode(myself, "-a") + RPL_UNAWAY(myself.Nickname))
			}

		case "PRIVMSG", "NOTICE", "SQUERY":
			myself.addSpamPenalty(1000)
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "PRIVMSG"))
			}
			if badCTCP(line) {
				continue
			}
			target := wordScanner.Text()
			c := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: c}
			receptor := <-c
			if receptor == nil {
				myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, target))
				continue
			}
			if msgs := strings.SplitN(line, ":", 2); len(msgs) != 2 {
				myself.rawOutput <- []byte(syntaxError(line))
				continue
			} else {
				receptor.SendMessage(myself.Nickname, []byte(PRIVMSG(myself.Nickname, target, msgs[1])))
			}

		case "PART":
			myself.addSpamPenalty(1000)
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "PART"))
				continue
			}
			channelList := wordScanner.Text()
			channelSlice := strings.SplitN(channelList, ",", 21)
			if len(channelSlice) >= 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
				continue
			}
			for _, target := range channelSlice {
				usersInChannelsMutex.RLock()
				ircChan, ok := usersInChannels[myself.Nickname][target]
				usersInChannelsMutex.RUnlock()
				if !ok {
					myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, target))
				} else {
					if ircChan.RefAttach() {
						ircChan.operation <- &ircChanOp{myself, "PART", myself.rawOutput}
						ircChan.RefDeattach()
					}
				}
			}

		case "JOIN": // lenient and forgiving interpretation of RFC2812 §3.2.1
			myself.addSpamPenalty(1000)
			var ok bool
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "JOIN"))
				continue
			}
			c := make(chan (namedIRCItem))
			channelList := wordScanner.Text()
			if strings.EqualFold(channelList, "0") {
				// the "part from all channels-command"
				myself.addSpamPenalty(5000)
				usersInChannelsMutex.Lock()
				channelsIamIn := make([]*ircChannel, len(usersInChannels[myself.Nickname]))
				i := 0
				for _, channel := range usersInChannels[myself.Nickname] {
					channelsIamIn[i] = channel
					i++
				}
				usersInChannelsMutex.Unlock()
				for _, channel := range channelsIamIn {
					if channel.RefAttach() {
						channel.operation <- &ircChanOp{User: myself, Command: "PART", ReplyTo: myself.rawOutput}
						channel.RefDeattach()
					}
				}
				continue
			}
			// slice of all channels to join
			channelSlice := strings.SplitN(channelList, ",", 21)
			if len(channelSlice) >= 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
				continue
			}
			// Slice of keys. As we iterate through the list of channels to join, we shall also
			// iterate through the list of channel keys, and use each key as a password to the
			// channel about to be joined. The list of keys may be shorter than the list of
			// channels to be joined, in which case the remaining list of channels will be
			// joined without using keys.
			keySlice := make([]string, 0)
			if wordScanner.Scan() {
				keySlice = strings.SplitN(wordScanner.Text(), ",", 21)
				if len(keySlice) >= 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
					continue
				}
			}
			for i, target := range channelSlice {
				myself.addSpamPenalty(500)
				NameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: c}
				receptor := <-c
				if receptor == nil {
					receptor, ok = createIRCChannel(target)
					if !ok {
						myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, target))
						continue
					}
				}
				joinCommand := "JOIN"
				if len(keySlice) > i {
					joinCommand += " " + keySlice[i]
				}
				switch t := receptor.(type) {
					case *ircChannel:
						if t.RefAttach() {
							t.operation <- &ircChanOp{myself, joinCommand, myself.rawOutput}
							t.RefDeattach()
						} else {
							myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, target))
						}
					default:
						myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, target))
				}
			}

		case "TOPIC":
			myself.addSpamPenalty(500)
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "TOPIC"))
				continue
			}
			channel := wordScanner.Text()
			c := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: channel, ReplyTo: c}
			receptor := <-c
			switch t := receptor.(type) {
			case *ircChannel:
				if t.RefAttach() {
					if commandAndComment := strings.SplitN(line, ":", 2); len(commandAndComment) != 1 {
						t.operation <- &ircChanOp{User: myself, Command: "TOPIC " + commandAndComment[1], ReplyTo: myself.rawOutput}
					} else {
						t.operation <- &ircChanOp{User: myself, Command: "TOPIC", ReplyTo: myself.rawOutput}
					}
					t.RefDeattach()
				}
			default:
				myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, channel))
			}

		case "NICK":
			myself.addSpamPenalty(5000)
			// -1. is the nickname valid?
			// 0. is desired nickname already taken? if not, take a mutex and...
			// 1. update the usersInChannels map
			// 2. update the userlist in each channel the user is in
			// 3. make a set of all users in each channel the user is in,
			//    where each user only occurs once, to avoid sending too
			//    many messages (see 4)
			// 4. send nick update messages to each user in that set
			// 5. update the NameSpace and release the mutex (see above)
			// 6. send nick update message to this user
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "NICK"))
				continue
			}
			desiredNick := wordScanner.Text()
			if !validNickname(desiredNick) {
				myself.rawOutput <- []byte(ERR_ERRONEUSNICKNAME(myself.Nickname, desiredNick))
				continue
			}
			if protectedNickname(desiredNick) {
				myself.rawOutput <- []byte(ERR_NICKNAMEINUSE(myself.Nickname, desiredNick))
				continue
			}
			cLookup := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: desiredNick, ReplyTo: cLookup}
			maybeSomeone := <-cLookup
			if maybeSomeone != nil {
				myself.rawOutput <- []byte(ERR_NICKNAMEINUSE(myself.Nickname, desiredNick))
				continue
			}
			usersInChannelsMutex.Lock()
			myChannelMap := usersInChannels[myself.Nickname]
			usersInChannels[desiredNick] = myChannelMap
			oldNick := myself.Nickname
			myself.Nickname = desiredNick
			delete(usersInChannels, oldNick)
			userList := ""
			cBytes := make(chan []byte)
			for _, ircChannel := range usersInChannels[myself.Nickname] {
				if ircChannel.RefAttach() {
					ircChannel.operation <- &ircChanOp{User: myself, Command: "NICK " + oldNick + " " + desiredNick, ReplyTo: cBytes}
					userList += string(<-cBytes)
					ircChannel.RefDeattach()
				}
			}
			strings.TrimSpace(userList)
			if userList != "" {
				userSet := make(map[string]bool)
				for _, name := range strings.Split(userList, " ") {
					userSet[name] = true
				}
				for name, _ := range userSet {
					NameSpace.Lookup <- &NSLookupRequest{Key: name, ReplyTo: cLookup}
					ircUser := <-cLookup
					if ircUser != nil {
						ircUser.SendMessage(myself.Nickname, []byte(NICK(oldNick, desiredNick)))
					}
				}
			}
			cOK := make(chan bool)
			NameSpace.Replace <- &NSReplaceRequest{Key: oldNick, NewKey: desiredNick, ReplyTo: cOK}
			<-cOK
			usersInChannelsMutex.Unlock()
			myself.rawOutput <- []byte(NICK(oldNick, desiredNick))

		case "MODE":
			myself.addSpamPenalty(500)
			// handles both channel and user mode; both requests and attempts at modifications
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "MODE"))
				continue
			}
			target := wordScanner.Text()
			cLookup := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: cLookup}
			modeCommand := ""
			for wordScanner.Scan() {
				modeCommand += wordScanner.Text() + " "
			}
			strings.TrimSpace(modeCommand)
			obj := <-cLookup
			if obj == nil || !obj.RefAttach() {
				myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, target))
				continue
			}
			switch t := obj.(type) {
			case *IRCUser:
				if modeCommand == "" {
					// GET mode of a user
					if t == myself {
						myself.rawOutput <- []byte(RPL_UMODEIS(myself.Nickname, myself.modeString()))
					} else if t != myself && !myself.IsOperator {
						myself.rawOutput <- []byte(ERR_USERSDONTMATCH(myself.Nickname))
					} else {
						// operators are able to see everyones actual real mode
						myself.rawOutput <- []byte(PRIVMSG("SERVER", myself.Nickname, fmt.Sprintf("User %s has MODE %s", t.Nickname, t.modeString())))
					}
				} else {
					// SET mode of a user
					if t != myself && !myself.IsOperator {
						myself.rawOutput <- []byte(ERR_USERSDONTMATCH(myself.Nickname))
						obj.RefDeattach()
						continue
					}
					// send message to user, to change mode
					cR := make(chan string)
					t.setMode <- &UserModeUpdate{User: myself, ModeCommand: modeCommand, ReplyTo: cR}
					modeMsg := <-cR
					myself.rawOutput <- []byte(modeMsg)
					if t != myself {
						t.rawOutput <- []byte(MODE(myself.Nickname, t.Nickname, modeCommand) + modeMsg)
					}
				}
			case *ircChannel:
				if modeCommand == "" {
					t.operation <- &ircChanOp{User: myself, Command: "MODE", ReplyTo: myself.rawOutput}
				} else {
					t.operation <- &ircChanOp{User: myself, Command: "MODE " + modeCommand, ReplyTo: myself.rawOutput}
				}
			default:
				myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, target))
			}
			obj.RefDeattach()

		case "NAMES":
			myself.addSpamPenalty(1000)
			if !wordScanner.Scan() {
				// user asks for a list of *all* users and channels. reply with just the channels the user is in.
				usersInChannelsMutex.RLock()
				for _, ircChan := range usersInChannels[myself.Nickname] {
					if ircChan.RefAttach() {
						ircChan.operation <- &ircChanOp{User: myself, Command: "NAMES", ReplyTo: myself.rawOutput}
						ircChan.RefDeattach()
					}
				}
				usersInChannelsMutex.RUnlock()
				myself.rawOutput <- []byte(RPL_NAMREPLY(myself.Nickname, "*", []string{"anonymous"}) + RPL_ENDOFNAMES(myself.Nickname, "*"))
				continue
			}
			targetList := wordScanner.Text()
			if wordScanner.Scan() && wordScanner.Text() != mcxconfig.GetFullName() {
				// do not forward this message to any other server
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
				continue
			}
			channelSlice := strings.SplitN(targetList, ",", 21)
			if len(channelSlice) > 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, targetList))
				continue
			}
			usersInChannelsMutex.RLock()
			myChannels := usersInChannels[myself.Nickname]
			for _, name := range channelSlice {
				if ircChan, ok := myChannels[name]; ok && ircChan.RefAttach() {
					ircChan.operation <- &ircChanOp{User: myself, Command: "NAMES", ReplyTo: myself.rawOutput}
					ircChan.RefDeattach()
				}
			}
			usersInChannelsMutex.RUnlock()

		case "LIST":
			myself.addSpamPenalty(500)
			rBytesChan := make(chan []byte)
			if !wordScanner.Scan() {
				// user asks for a list of *all* channels.

				channels := make(map[string]bool)
				usersInChannelsMutex.RLock()
				for _, user2chans := range usersInChannels {
					for _, channel := range user2chans {
						if _,alreadyDone := channels[channel.name]; !alreadyDone {
							if channel.RefAttach() {
								channel.operation <- &ircChanOp{User: myself, Command: "LIST", ReplyTo: rBytesChan}
								myself.rawOutput <- <-rBytesChan
								channel.RefDeattach()
							}
						}
						channels[channel.name] = true
					}
				}
				usersInChannelsMutex.RUnlock()
				myself.rawOutput <- []byte(RPL_LISTEND(myself.Nickname))
				continue
			}
			chanList := wordScanner.Text()
			if wordScanner.Scan() && wordScanner.Text() != mcxconfig.GetFullName() {
				// do not forward this message to any other server
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
				continue
			}
			chanSlice := strings.SplitN(chanList, ",", 21)
			if len(chanSlice) > 20 {
				// actually ERR_TOOMANYMATCHES but that msg is not described in the RFC (DERP!?)
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, chanList))
				continue
			}
			usersInChannelsMutex.RLock()
			myChannels := usersInChannels[myself.Nickname]
			for _, name := range chanSlice {
				if ircChan, ok := myChannels[name]; ok && ircChan.RefAttach() {
					ircChan.operation <- &ircChanOp{User: myself, Command: "LIST", ReplyTo: rBytesChan}

					// if we don't bounce this message, RPL_LISTEND might be sent before the list is at the end
					myself.rawOutput <- <-rBytesChan
					ircChan.RefDeattach()
				}
			}
			usersInChannelsMutex.RUnlock()
			myself.rawOutput <- []byte(RPL_LISTEND(myself.Nickname))

		case "KICK":
			myself.addSpamPenalty(500)
			// The syntax of this command is utterly retarded. See RFC2812 §3.2.8.
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "KICK"))
				continue
			}
			chanList := wordScanner.Text()
			channelSlice := strings.SplitN(chanList, ",", 21)
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "KICK"))
				continue
			}
			userSlice := strings.SplitN(wordScanner.Text(), ",", 21)

			// decode kick comment if possible, using the special scanner-cutter
			comment := ""
			if wordScanner.Scan() {
				comment = string(cutter.CUT())
				if(rune(comment[0]) == ':') {
					comment = strings.TrimSpace(comment[1:])
				}
			}

			if len(userSlice) > 1 && len(channelSlice) > 1 && len(userSlice) != len(channelSlice) {
				myself.rawOutput <- []byte(syntaxError(line))
				continue
			}
			if len(channelSlice) > 20 || len(userSlice) > 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, chanList))
				continue
			}
			if len(channelSlice) > 1 {
				// Backwards compability case: The length of the channel list and the user list is equal
				for i, channel := range channelSlice {
					usersInChannelsMutex.RLock()
					if ircChan, present := usersInChannels[myself.Nickname][channel]; present && ircChan.RefAttach() {
						usersInChannelsMutex.RUnlock()
						kickCommand := "KICK " + userSlice[i]
						if comment != "" {
							kickCommand += " :" + comment
						}
						ircChan.operation <- &ircChanOp{User: myself, Command: kickCommand, ReplyTo: myself.rawOutput}
						ircChan.RefDeattach()
					} else {
						myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, channel))
					}
				}
			} else {
				// The normal case: One or multiple douchebags needs to be evicted from a channel
				usersInChannelsMutex.RLock()
				ircChan, present := usersInChannels[myself.Nickname][channelSlice[0]]
				usersInChannelsMutex.RUnlock()
				if !present || !ircChan.RefAttach() {
					myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, channelSlice[0]))
					continue
				} else {
					for _, douchebag := range userSlice {
						kickCommand := "KICK " + douchebag
						if comment != "" {
							kickCommand += " :" + comment
						}
						ircChan.operation <- &ircChanOp{User: myself, Command: kickCommand, ReplyTo: myself.rawOutput}
						ircChan.RefDeattach()
					}
				}
			}

		case "INFO":
			// According to RFC2812 §3.4.10, we are REQUIRED to implement this command.
			if wordScanner.Scan() && wordScanner.Text() != mcxconfig.GetFullName() {
				// do not forward this message to any other server
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
				continue
			}
			myself.rawOutput <- []byte(RPL_INFO(myself.Nickname) + RPL_ENDOFINFO(myself.Nickname))

		case "MOTD":
			if wordScanner.Scan() && wordScanner.Text() != mcxconfig.GetFullName() {
				// do not forward this message to any other server
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
				continue
			}
			myself.rawOutput <- []byte(RPL_MOTDSTART(myself.Nickname) + RPL_MOTD(myself.Nickname) + RPL_ENDOFMOTD(myself.Nickname))

		case "WHO":
			myself.addSpamPenalty(500)
			// Some IRC clients use WHO to get a list of everyone in a channel.
			if !wordScanner.Scan() || wordScanner.Text() == "0" {
				// user asks for a list of *all* users logged in. we return an empty list.
				myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, "*"))
				continue
			}
			target := wordScanner.Text()
			if wordScanner.Scan() {
				if wordScanner.Text() == "o" {
					// User asks to see opers only. Do not comply.
					myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, target))
					continue
				} else {
					myself.rawOutput <- []byte(syntaxError(line))
					continue
				}
			}
			cLookup := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: cLookup}
			switch t := (<-cLookup).(type) {
			case *ircChannel:
				if t.RefAttach() {
					t.operation <- &ircChanOp{User: myself, Command: "WHO", ReplyTo: myself.rawOutput}
					t.RefDeattach()
				} else {
					myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, target))
				}
			case *IRCUser:
				myself.rawOutput <- []byte(RPL_SIMPLEWHOREPLY(myself.Nickname, "*", target, false) + RPL_ENDOFWHO(myself.Nickname, target))
			default:
				myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, target))
			}

		case "VERSION":
			if wordScanner.Scan() && wordScanner.Text() != mcxconfig.GetFullName() {
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
				if wordScanner.Scan() {
					myself.rawOutput <- []byte(syntaxError(line))
				}
				continue
			}
			myself.rawOutput <- []byte(RPL_VERSION(myself.Nickname))

		case "WHOIS":
			remoteServer := ""
			whoList := ""
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(syntaxError(line))
				continue
			}
			whoList = wordScanner.Text()
			if wordScanner.Scan() {
				remoteServer = whoList
				whoList = wordScanner.Text()
				if wordScanner.Scan() {
					// 3rd argument? not allowed!
					myself.rawOutput <- []byte(syntaxError(line))
					continue
				}
			}
			if whoList == "*" || remoteServer == "*" {
				myself.rawOutput <- []byte(RPL_ENDOFWHOIS(myself.Nickname, "*"))
				continue
			}
			if remoteServer != "" && remoteServer != mcxconfig.GetFullName() {
				myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, remoteServer))
				continue
			}
			whoSlice := strings.SplitN(whoList, ",", 21)
			if len(whoSlice) > 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, whoList))
				continue
			}
			cLookup := make(chan (namedIRCItem))
			for i := 0; i != len(whoSlice); i++ {
				NameSpace.Lookup <- &NSLookupRequest{Key: whoSlice[i], ReplyTo: cLookup}
				switch (<-cLookup).(type) {
					case *IRCUser:
						myself.rawOutput <- []byte(RPL_SIMPLEWHOISUSER(myself.Nickname, whoSlice[i]) + RPL_ENDOFWHOIS(myself.Nickname, whoSlice[i]))
					default:
						myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, whoSlice[i]) + RPL_ENDOFWHOIS(myself.Nickname, whoSlice[i]))
				}
			}
		case "WHOWAS":
			myself.addSpamPenalty(5000)
			// always reply with "there never was no such a user", no matter what the input is.
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "WHOWAS"))
				continue
			}
			whoSlice := strings.SplitN(wordScanner.Text(), ",", 21)
			if len(whoSlice) > 20 {
				myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, strings.Join(whoSlice, " ")))
				continue
			}
			for _, nick := range whoSlice {
				myself.rawOutput <- []byte(ERR_WASNOSUCHNICK(myself.Nickname, nick))
			}

		case "OPER":
			myself.addSpamPenalty(20000)
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
				continue
			}
			username := wordScanner.Text()
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
				continue
			}
			password := wordScanner.Text()
			if mcxconfig.OperLogin(username, password) {
				myself.IsOperator = true
				myself.rawOutput <- []byte(RPL_YOUREOPER(myself.Nickname) + MODE("anonymous", myself.Nickname, "+o") + RPL_UMODEIS(myself.Nickname, myself.modeString()))
				log.Printf("MCXIRCD: User \"%s\" scuccessfully identified as system operator", myself.Nickname)
				myself.SetSpamProtection(false) // opers are given the fast lane
				continue
			}
			myself.rawOutput <- []byte(ERR_PASSWDMISMATCH(myself.Nickname) + PRIVMSG("SERVER", myself.Nickname, "Your attempt to gain sysop status has been notified."))
			log.Printf("MCXIRCD: User \"%s\" supplied wrong password while trying to gain system operator status", myself.Nickname)

		case "KILL":
			if !myself.IsOperator {
				myself.rawOutput <- []byte(ERR_NOPRIVILEGES(myself.Nickname))
				continue
			}
			if !wordScanner.Scan() {
				myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
				continue
			}
			whomToKill := wordScanner.Text()
			cLookup := make(chan (namedIRCItem))
			NameSpace.Lookup <- &NSLookupRequest{Key: whomToKill, ReplyTo: cLookup}
			switch t := (<-cLookup).(type) {
			case *IRCUser:
				if t.RefAttach() {
					t.Kill <- true
					t.RefDeattach()
				}
			default:
				myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, whomToKill))
			}

		case "USER":
			continue // ignore

		case "REHASH":
			if !myself.IsOperator {
				myself.rawOutput <- []byte(ERR_NOPRIVILEGES(myself.Nickname))
				continue
			}
			myself.rawOutput <- []byte(PRIVMSG("SERVER", myself.Nickname, "REHASH BEGIN") + RPL_REHASHING(myself.Nickname))
			statusMsgs, _ := mcxconfig.REHASH()
			for _,str := range statusMsgs {
				myself.rawOutput <- []byte(PRIVMSG("SERVER", myself.Nickname, str))
			}
			myself.rawOutput <- []byte(PRIVMSG("SERVER", myself.Nickname, "REHASH COMPLETE") + RPL_REHASHING(myself.Nickname))

		default:
			myself.addSpamPenalty(100)
			myself.rawOutput <- []byte(syntaxError(line))
		}
	}
}



func syntaxError(line string) string {
	return fmt.Sprintf("INFO :Bad input or unimplemented command. You said: " + line + "\r\n")
}



// Implements namedIRCItem
func (myself *IRCUser) Identifier() string {
	return myself.Nickname
}



// Implements namedIRCItem
func (myself *IRCUser) SendMessage(from string, bytes []byte) bool {
	if myself.RefAttach() {
		myself.rawOutput <- bytes
		myself.RefDeattach()
		return true
	}
	return false
}



// Implements namedIRCItem
func (myself *IRCUser) RefAttach() bool {
	myself.referenceCounterMutex.Lock()
	if myself.referenceCounter == 0 {
		return false
	}
	myself.referenceCounter += 1
	myself.referenceCounterMutex.Unlock()
	return true
}



// Implements namedIRCItem
func (myself *IRCUser) RefDeattach() {
	myself.referenceCounterMutex.Lock()
	myself.referenceCounter -= 1
	myself.referenceCounterMutex.Unlock()
}



// Removes the IRC user from a lot of internal datastructures.
func (myself *IRCUser) cleanDestroy() {

	// make list of channels I am in
	usersInChannelsMutex.RLock()
	myChans := make([]*ircChannel, len(usersInChannels[myself.Nickname]))
	i := 0
	for _, ircChan := range usersInChannels[myself.Nickname] {
		myChans[i] = ircChan
		i++
	}
	usersInChannelsMutex.RUnlock()

	// part from all those channels
	replyBytes := make(chan []byte)
	for _, ircChan := range myChans {
		if ircChan.RefAttach() {
			ircChan.operation <- &ircChanOp{User: myself, Command: "PART", ReplyTo: replyBytes}
			<-replyBytes
			ircChan.RefDeattach()
		}
	}

	// remove my name from the map of users->channels
	usersInChannelsMutex.Lock()
	delete(usersInChannels, myself.Nickname)
	usersInChannelsMutex.Unlock()


	// delete my name from the NameSpace
	reply := make(chan bool)
	NameSpace.Del <- &NSDelRequest{Key: myself.Nickname, ReplyTo: reply}
	<-reply


	// kill the message-passing goroutine
	myself.die <- true
}
