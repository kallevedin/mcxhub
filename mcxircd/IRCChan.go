package mcxircd

/*

An IRC Channel consist of two things: The ircChan struct, and the ircChanLoop()-
function, where a goroutine is living. Do everything through message passing via
channels, and not by writing to the ircChan struct!!!

Important types:
ircChanOP -- used by IRCUser's to send messages like JOIN, TOPIC, PART, KICK, MODE...
chanOPMsg -- used for channel-to-channel messages. (Twin channels, when in mode +A)
ircdistribute -- used to distribute raw []byte's to everyone in the channel. (Only PRIVMSG's currently, but not limited to that.)



When sending messages to an ircChan over channels: You need to make sure that 
the ircChan does not die before receiving on the channel, and optionally also
that it replies. You need to tell the ircChan not to die before it has processed 
your message!

You do this by calling RefAttach(). When you are done with sending messages with
the ircChan, you call RefDeattach(). Like so:

if ircChan.RefAttach() {
	ircChan.someChannel <- someMessage
	...
	ircChan.RefDeattach()
} else {
	// ircChan is in DYING mode. You SHOULD NOT send messages to it.
}

*/


import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"bufio"
	"bytes"
	"fmt"
	"regexp"
	"strings"
	"sync"
	"time"
)



// Channel operation message. Use this for sending operations to be performed at/in/on channels.
type ircChanOp struct {
	User    *IRCUser
	Command string
	ReplyTo chan ([]byte)
}



// Used to send operations between channels (used only by channel twins)
type chanOPMsg struct {
	From	*ircChannel
	Command	string
	ReplyTo	chan (*chanOPMsg)
}



// Channel distribution message. Put whatever you want (a PRIVMSG!!!) in Message, and then send 
// this message to a channel. It will then be distributed to all users in the channel.
type ircdistribute struct {
	From    string
	Message []byte
}



// The structure that defines a channel. Never write directly to this structure 
// if you don't "own" it. This goes hand-in-hand with the goroutine that runs
// in ircChanLoop().
type ircChannel struct {
	name		string
	users		map[string]*IRCUser
	operators	map[string]*IRCUser
	banned		map[string]*regexp.Regexp
	chanOP		chan (*chanOPMsg)
	distribute	chan (*ircdistribute)
	operation	chan (*ircChanOp)
	topic		string

	// used when the channel kills itself, to remove the last references pointing to it so that
	// it does not die while someone is expecting it to send replies through channels
	deathTicker				(<- chan time.Time)
	referenceCounterMutex	sync.RWMutex
	referenceCounter		int

	// twin channel, used by mode +A (linkAnon)
	twinChannel			*ircChannel

	// password for channel (toggleable by mode +/-k)
	channelKey			string

	// channel flags
	anonymous			bool
	noExternalMessages	bool
	topicLock			bool
	linkAnon			bool
}




// Creates a new IRC channel. Returns the channel itself, and true, if successful.
func createIRCChannel(name string) (*ircChannel, bool) {
	if validChannelNames.Match([]byte(name)) {
		c := ircChannel{name: name, 
			users: make(map[string]*IRCUser), 
			operators: make(map[string]*IRCUser), 
			banned: make(map[string]*regexp.Regexp), 
			chanOP: make(chan *chanOPMsg), 
			distribute: make(chan *ircdistribute), 
			operation: make(chan *ircChanOp), 
			topic: "", referenceCounter: 1, 
			twinChannel: nil, 
			channelKey: ""}		
		c.initChannelMode()
		registerNameOk := make(chan bool)
		NameSpace.Add <- &NSAddRequest{Key: c.name, Value: &c, ReplyTo: registerNameOk}
		if registerOK := <-registerNameOk; registerOK {
			go c.ircChanLoop()
			return &c, true
		}
	}
	return nil, false
}



// Helper function that takes care of setting the correct mode flags for a
// channel when it is created.
func (myself *ircChannel) initChannelMode() {
	switch myself.channelType() {
	case '#':
		myself.anonymous = false
		myself.noExternalMessages = true
		myself.topicLock = false
		myself.linkAnon = false
	case '+':
		myself.anonymous = true
		myself.noExternalMessages = true
		myself.topicLock = true
		myself.linkAnon = false
	}
}



// Message-passing function for the IRC channel! Every channel has its own goroutine that executes this. 
// Everything you do with a channel is sent to it, and processed, by this function/goroutine.
func (myself *ircChannel) ircChanLoop() {
	// when an anonymous (+-channel) is created, it announces its existence to its twin, in case they should merge
	if myself.channelType() == '+' {
		c := make(chan namedIRCItem)
		NameSpace.Lookup <- &NSLookupRequest{Key: myself.twinChannelName(), ReplyTo: c}
		obj := <-c
		switch t := obj.(type) {
			case *ircChannel :
				if t.RefAttach() {
					t.chanOP <- &chanOPMsg{From: myself, Command: "TWIN_ANNOUNCE", ReplyTo: myself.chanOP}
					t.RefDeattach()
				}
		}
	}
	for {
		select {

		// this channel is created when the IRC-channel kills itself. All references to the channel is removed
		// from datastructures, and then it waits for other goroutines to drop its references. Then it returns
		// and dies for real. After this, the garbage collecter will do its work ;)
		case <-myself.deathTicker :
			if myself.referenceCounter == 0 {
				return
			}

		case req := <-myself.chanOP :
			myself.processChanOP(req)

		case message := <-myself.distribute :
			if myself.twinChannel != nil && message.From == myself.twinChannel.name {
				for _, ircUser := range myself.users {
					if ircUser.RefAttach() {
						ircUser.rawOutput <- message.Message
						ircUser.RefDeattach()
					}
				}
				continue
			} 
			if myself.noExternalMessages {
				if _, present := myself.users[message.From]; !present {
					continue
				}
			}
			if myself.anonymous && privmsgRegexp.Match(message.Message) {
				msg := bytes.SplitN(message.Message, []byte(":"), 3)
				if len(msg) == 3 {
					message.Message = []byte(":anonymous!anonymous@" + mcxconfig.GetHostcloak() + " PRIVMSG " + myself.name + " :" + string(msg[2]))
				}
			}
			for nickname, ircUser := range myself.users {
				if nickname != message.From && ircUser.RefAttach() {
					ircUser.rawOutput <- message.Message
					ircUser.RefDeattach()
				}
			}
			if myself.twinChannel != nil {
				twinMessage := bytes.Replace(message.Message, []byte(myself.name), []byte(myself.twinChannelName()), 1)
				myself.twinChannel.SendMessage(myself.name, twinMessage)
			}
			
		case req := <-myself.operation:
			myself.processOperation(req)
		}
	}
}



// Processes messages sent from channels
func (myself *ircChannel) processChanOP(req *chanOPMsg) {
	if !req.From.RefAttach() {
		return
	}
	defer req.From.RefDeattach()
	switch req.Command {
		case "TWIN_ANNOUNCE" :
			if myself.linkAnon {
				myself.attachToTwinChannel()
			}
		case "TWIN_ATTACH" :
			if myself.twinChannel == nil {
				myself.twinChannel = req.From
				req.ReplyTo <- &chanOPMsg{From: myself, Command: "OK", ReplyTo: nil}
			} else {
				req.ReplyTo <- &chanOPMsg{From: myself, Command: "FAIL", ReplyTo: nil}
			}
		case "TWIN_DEATTACH" :
			if myself.twinChannel == req.From {
				myself.twinChannel = nil
				req.ReplyTo <- &chanOPMsg{From: myself, Command: "OK", ReplyTo: nil}
			} else {
				req.ReplyTo <- &chanOPMsg{From: myself, Command: "FAIL", ReplyTo: nil}
			}
		default :
			panic(fmt.Sprintf("Unknown message sent to %s.chanOP(). From: %s, Command: %s", myself.name, req.From.name, req.Command))
	}
}



// handles requests sent to the channel, from users wanting to interact with it in various ways.
func (myself *ircChannel) processOperation(req *ircChanOp) {
	// protect against the user message-handling goroutine dies before we have returned the reply
	if !req.User.RefAttach() {
		return
	}
	defer req.User.RefDeattach()
	
	scanner := bufio.NewScanner(strings.NewReader(req.Command))
	scanner.Split(bufio.ScanWords)
	if !scanner.Scan() {
		panic(fmt.Sprintf("%#v\n", req))
	}
	command := scanner.Text()

	switch command {
	
	case "JOIN" :
		if myself.isBanned(req.User.Nickname) {
			req.ReplyTo <- []byte(ERR_BANNEDFROMCHAN(req.User.Nickname, myself.name))
			return
		}
		if myself.channelKey != "" {
			if !scanner.Scan() || scanner.Text() != myself.channelKey {
				req.ReplyTo <- []byte(ERR_BADCHANNELKEY(req.User.Nickname, myself.name))
				return
			}
		}
		// op the first user joining the channel, if this is an ordinary channel
		if myself.channelType() == '#' && len(myself.users) == 0 {
			myself.operators[req.User.Nickname] = req.User
		}
		_,alreadyJoined := myself.users[req.User.Nickname]
		if !alreadyJoined {
			myself.users[req.User.Nickname] = req.User
			usersInChannelsMutex.Lock()
			usersInChannels[req.User.Nickname][myself.name] = myself
			usersInChannelsMutex.Unlock()
		}
		if !myself.anonymous {
			// channel is not anonymous, so we dump the list of attendees
			nicknames := make([]string, len(myself.users))
			i := 0
			for name := range myself.users {
				if _, isOp := myself.operators[name]; isOp {
					nicknames[i] = "@" + name
				} else {
					nicknames[i] = name
				}
				i++
			}
			req.ReplyTo <- []byte(
				JOIN(req.User.Nickname, myself.name) +
					RPL_NAMREPLY(req.User.Nickname, myself.name, nicknames) +
					RPL_ENDOFNAMES(req.User.Nickname, myself.name) +
					RPL_MAYBETOPIC(req.User.Nickname, myself.name, myself.topic) +
					RPL_CHANNELMODEIS(req.User.Nickname, myself.name, myself.modeString()))
			if !alreadyJoined {
				for _, ircUser := range myself.users {
					if ircUser != req.User && ircUser.RefAttach() {
						ircUser.rawOutput <- []byte(JOIN(req.User.Nickname, myself.name))
						ircUser.RefDeattach()
					}
				}
			}
		} else {
			// channel is anonymous and so we censor the output here
			fakeList := make([]string, 2)
			fakeList[0] = "anonymous"
			fakeList[1] = req.User.Nickname
			req.ReplyTo <- []byte(
				JOIN(req.User.Nickname, myself.name) +
					RPL_NAMREPLY(req.User.Nickname, myself.name, fakeList) +
					RPL_ENDOFNAMES(req.User.Nickname, myself.name) +
					RPL_CHANNELMODEIS(req.User.Nickname, myself.name, myself.modeString()) +
					PRIVMSG("anonymous", myself.name, "This channel is anonymous. Everyone in this channel will appear as me."))
		}
	
	case "PART" :
		if _, ok := myself.users[req.User.Nickname]; !ok {
			req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.name))
			return
		}
		msg := []byte(PART(req.User.Nickname, myself.name))
		delete(myself.users, req.User.Nickname)
		delete(myself.operators, req.User.Nickname)
		if !myself.anonymous {
			for _, user := range myself.users {
				if user != req.User && user.RefAttach() {
					user.rawOutput <- msg
					user.RefDeattach()
				}
			}
		}
		req.ReplyTo <- msg // this ...
		// ... has to happen after ...
		usersInChannelsMutex.Lock() // ... this
		delete(usersInChannels[req.User.Nickname], myself.name)
		usersInChannelsMutex.Unlock()
		if len(myself.users) == 0 {
			myself.cleanDestroy()	// channel will now start destroying itself
		}
	
	case "TOPIC" :
		if _, ok := myself.users[req.User.Nickname]; !ok {
			req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.name))
			return
		}
		if !scanner.Scan() {
			// get topic
			req.ReplyTo <- []byte(RPL_MAYBETOPIC(req.User.Nickname, myself.name, myself.topic))
			return
		} else {
			// set topic
			if myself.topicLock {
				if _, present := myself.operators[req.User.Nickname]; !present {
					req.ReplyTo <- []byte(ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.name))
					return
				}
			}
			newtopic := strings.SplitN(req.Command, " ", 2)[1]
			myself.topic = newtopic
			for _, user := range myself.users {
				if user.RefAttach() {
					user.rawOutput <- []byte(TOPIC(req.User.Nickname, myself.name, newtopic))
					user.RefDeattach()
				}
			}
		}
	
	case "NICK" :
		/*
			Syntax: "NICK oldNick newNick"
			Replies with a list of users in the channel; Because each user should receive ONE nick-update
			meassage each, and as the ircUser iterates through all the channels, and updates his nick in
			each one of them, each other user that is in *any* of the channels the user is in shall receive
			exactly one nick update message...
		*/
		scanner.Scan()
		oldNick := scanner.Text()
		scanner.Scan()
		newName := scanner.Text()
		delete(myself.users, oldNick)
		delete(myself.operators, oldNick)
		myself.users[newName] = req.User
		myself.operators[newName] = req.User
		userList := ""
		for nickname, ircUser := range myself.users {
			if req.User != ircUser {
				userList += nickname + " "
			}
		}
		req.ReplyTo <- []byte(userList)
	
	case "MODE" :
		if !scanner.Scan() {
			// get mode
			req.ReplyTo <- []byte(RPL_CHANNELMODEIS(req.User.Nickname, myself.name, myself.modeString()))
			return
		} else {
			// set mode, OR ask for extended information about the channel, which is covered in the switch-case below:
			newMode := strings.TrimSpace(strings.SplitN(req.Command, " ", 2)[1])
			if len(newMode) == 1 {
				switch newMode {
				case "b":
					req.ReplyTo <- []byte(myself.makeBanListReply(req.User.Nickname))
					return
				case "e":
					req.ReplyTo <- []byte(RPL_ENDOFEXCEPTLIST(req.User.Nickname, myself.name))
					return
				case "I":
					req.ReplyTo <- []byte(RPL_ENDOFINVITELIST(req.User.Nickname, myself.name))
					return
				case "O":
					// this case appears not to be covered in RFC2812 /!\ weird
					req.ReplyTo <- []byte("DERP_ERROR :DON'T ASK THAT QUESTION\r\n")
					return
				}
			}
			// so if it was not one of those three cases, it most likely is a set mode-operation...
			if !req.User.IsOperator {
				if _, isOp := myself.operators[req.User.Nickname]; !isOp {
					req.ReplyTo <- []byte(ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.name))
					return
				}
			}
			if msg, ok := myself.modifyMode(newMode, req.User.Nickname); !ok {
				req.ReplyTo <- []byte(msg)
				return
			}
			msg := []byte(MODE(req.User.Nickname, myself.name, newMode))
			for _, ircUser := range myself.users {
				if ircUser != req.User && ircUser.RefAttach() {
					ircUser.rawOutput <- msg
					ircUser.RefDeattach()
				}
			}
			req.ReplyTo <- msg
		}
	
	case "NAMES" :
		if _, present := myself.users[req.User.Nickname]; !present {
			req.ReplyTo <- []byte("")
			return
		}
		if myself.anonymous {
			req.ReplyTo <- []byte(RPL_NAMREPLY(req.User.Nickname, myself.name, []string{"anonymous", req.User.Nickname}) + RPL_ENDOFNAMES(req.User.Nickname, myself.name))
			return
		}
		userList := make([]string, len(myself.users))
		i := 0
		for nick, _ := range myself.users {
			if _, isOp := myself.operators[nick]; isOp {
				userList[i] = "@" + nick
			} else {
				userList[i] = nick
			}
			i++
		}
		req.ReplyTo <- []byte(RPL_NAMREPLY(req.User.Nickname, myself.name, userList) + RPL_ENDOFNAMES(req.User.Nickname, myself.name))
	
	case "LIST" :
		//if _, present := myself.users[req.User.Nickname]; !present {
		//	req.ReplyTo <- []byte("")
		//	return
		//}
		if myself.anonymous {
			req.ReplyTo <- []byte(RPL_LIST(req.User.Nickname, myself.name, myself.topic, 9001))
		} else {
			req.ReplyTo <- []byte(RPL_LIST(req.User.Nickname, myself.name, myself.topic, len(myself.users)))
		}
	
	case "KICK" :
		// syntax: "KICK whom" OR "KICK whom :multi-word comment"
		if _, present := myself.users[req.User.Nickname]; !present {
			req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.name))
			return
		}
		if _, isOp := myself.operators[req.User.Nickname]; !isOp {
			req.ReplyTo <- []byte(ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.name))
			return
		}
		if !scanner.Scan() {
			panic("CHANNEL KICK without anyone to kick")
		}
		whom := scanner.Text()
		if _, present := myself.users[whom]; !present {
			req.ReplyTo <- []byte(ERR_USERNOTINCHANNEL(req.User.Nickname, whom, myself.name))
			return
		}
		// extract comment if possible
		comment := ""
		coms := strings.SplitN(req.Command, ":", 2)
		if len(coms) == 2 {
			comment = coms[1]
		}
		delete(myself.operators, whom)
		usersInChannelsMutex.Lock()
		delete(usersInChannels[whom], myself.name)
		usersInChannelsMutex.Unlock()
		for _, ircUser := range myself.users {
			if ircUser.RefAttach() {
				ircUser.rawOutput <- []byte(KICK(req.User.Nickname, whom, myself.name, comment))
				ircUser.RefDeattach()
			}
		}
		delete(myself.users, whom)
	
	case "WHO" :
		// syntax: "WHO"
		if _, present := myself.users[req.User.Nickname]; !present {
			req.ReplyTo <- []byte(RPL_ENDOFWHO(req.User.Nickname, myself.name))
			return
		}
		var rply []string
		if myself.anonymous {
			rply = make([]string, 2)
			rply[0] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.name, "anonymous", false)
			rply[1] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.name, req.User.Nickname, myself.hasOp(req.User.Nickname))
		} else {
			rply = make([]string, len(myself.users))
			i := 0
			for nick, _ := range myself.users {
				rply[i] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.name, nick, myself.hasOp(nick))
				i++
			}
		}
		req.ReplyTo <- []byte(strings.Join(rply, "") + RPL_ENDOFWHO(req.User.Nickname, myself.name))
	}
}



// Parses a mode change string, and if everything is alrighty, updates the
// channels mode. Returns true if successful, or false and a string containing
// important information that is suitable for sending to an IRCUser, if 
// unsuccessful. See RFC2812 §3.2.3 for more information.
func (myself *ircChannel) modifyMode(modString, nickname string) (string, bool) {
	linkAnon := myself.linkAnon
	noExternalMessages := myself.noExternalMessages
	topicLock := myself.topicLock
	channelKey := myself.channelKey
	mod := true

	words := bufio.NewScanner(strings.NewReader(modString))
	words.Split(bufio.ScanWords)
	for words.Scan() {
		s := bufio.NewScanner(strings.NewReader(words.Text()))
		s.Split(bufio.ScanRunes)
		for s.Scan() {
			r := s.Text()
			switch r {
			case "+":
				mod = true
			case "-":
				mod = false
			case "A":
				linkAnon = mod
				if myself.channelType() == '#' {
					if mod && !myself.linkAnon {
						myself.attachToTwinChannel()
					} else if myself.linkAnon {
						myself.deattachFromTwinChannel()
					}
				}
			case "b":
				if !words.Scan() {
					return ERR_NEEDMOREPARAMS(nickname, "MODE"), false
				}
				banString := words.Text()
				if mod { // add ban
					// check if the ban is valid
					reg,ok := makeBanRegexp(banString)
					if !ok {
						return fmt.Sprintf("INFO: Bad ban mask %s\r\n", banString), false
						continue
					}
					// add ban and run it against all users
					myself.banned[banString] = reg
					for name, _ := range myself.users {
						if reg.MatchString(name) {
							for _, ircUser := range myself.users {
								if ircUser.RefAttach() {
									ircUser.rawOutput <- []byte(KICK(nickname, name, myself.name, "excommunication"))
									ircUser.RefDeattach()
								}
							}
							delete(myself.users, name)
							delete(myself.operators, name)
							usersInChannelsMutex.Lock()
							delete(usersInChannels[name], myself.name)
							usersInChannelsMutex.Unlock()
							if len(myself.users) == 0 {
								// if *everyone* was banned and evicted from the channel, 
								// the channel is empty, and it destroys itself!
								myself.cleanDestroy()
							}
						}
					}
				} else { // remove ban
					delete(myself.banned, banString)
				}
			case "k":
				if mod {
					if !words.Scan() {
						return ERR_NEEDMOREPARAMS(nickname, "MODE"), false
					}
					channelKey = words.Text()
				} else {
					channelKey = ""
				}
			case "n":
				noExternalMessages = mod
			case "o":
				if !words.Scan() {
					return ERR_NEEDMOREPARAMS(nickname, "MODE"), false
				}
				target := words.Text()
				user, isPresent := myself.users[target]
				if !isPresent {
					// can not OP a nonpresent user
					// the IRC protocol does not cover this case, so treat it as a NOP
					continue
				}
				if mod {
					myself.operators[target] = user
				} else {
					delete(myself.operators, target)
				}
			case "t":
				topicLock = mod
			default:
				return ERR_UNKNOWNMODE(nickname, myself.name, r), false
			}
		}
		if err := s.Err(); err != nil {
			return ERR_NEEDMOREPARAMS(nickname, "MODE"), false
		}
	}
	if err := words.Err(); err != nil {
		return ERR_NEEDMOREPARAMS(nickname, "MODE"), false
	}
	myself.linkAnon = linkAnon
	myself.noExternalMessages = noExternalMessages
	myself.topicLock = topicLock
	myself.channelKey = channelKey
	return "", true
}


// Implements namedIRCItem
func (myself *ircChannel) SendMessage(from string, bytes []byte) bool {
	if myself.RefAttach() {
		myself.distribute <- &ircdistribute{From: from, Message: bytes}
		myself.RefDeattach()
		return true
	}
	return false
}



// Implements namedIRCItem
func (myself *ircChannel) Identifier() string {
	return myself.name
}


// Implements IRCNamedItem
func (myself *ircChannel) RefAttach() bool {
    myself.referenceCounterMutex.Lock()
    if myself.referenceCounter == 0 {
        return false
    }   
    myself.referenceCounter += 1
    myself.referenceCounterMutex.Unlock()
    return true
}



// Implements IRCNamedItem
func (myself *ircChannel) RefDeattach() {
    myself.referenceCounterMutex.Lock()
    myself.referenceCounter -= 1
    myself.referenceCounterMutex.Unlock()
}



func (myself *ircChannel) cleanDestroy() {
	myself.deattachFromTwinChannel()
	usersInChannelsMutex.Lock()
	for _, user := range myself.users { // this will probably be an empty list almost always, or always
		delete(usersInChannels[user.Nickname], myself.name)
		if user.RefAttach() {
			user.rawOutput <- []byte(PART(user.Nickname, myself.name))
			user.RefDeattach()
		}
	}
	usersInChannelsMutex.Unlock()

	reply := make(chan bool)
	request := &NSDelRequest{Key: myself.name, ReplyTo: reply}
	NameSpace.Del <- request
	<-reply
	myself.RefDeattach()	// remove the last references to the channel
	myself.deathTicker = time.Tick(1 * time.Second) // go into dying mod
}
