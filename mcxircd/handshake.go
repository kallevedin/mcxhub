package mcxircd

import (
	"bufio"
	"net"
	"strings"
	"time"
)

// handshake performs handshakes with connecting entities. In theory it figures
// out if the connecting entity is a user, a server, a service or something
// else, and creates such an object after the handshake is complete, but right
// now it can only handle users.
func Handshake(conn net.Conn) (*IRCUser, bool) {
	failure := true
	defer handshakeCleanup(conn, &failure)
	conn.SetReadDeadline(time.Now().Add(TIMEOUT_HANDSHAKE * time.Second))

	scanner := bufio.NewScanner(conn) // splits input at \r\n OR \n
	for {
		if !scanner.Scan() {
			err := scanner.Err()
			if err == nil { // connection closed
				return nil, false
			} else {
				switch t := err.(type) {
				case net.Error:
					if t.Timeout() {
						conn.Write([]byte("ERROR: Handshake timeout\r\n"))
						return nil, false
					}
				default:
					return nil, false
				}
			}
		}

		line := scanner.Text()
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		wordScanner.Split(bufio.ScanWords)

		if !wordScanner.Scan() {
			continue
		}
		command := wordScanner.Text()
		switch command {
		// This is very accepting and forgiving!!!

		case "CAP":
			continue

		case "PASS":
			continue

		case "USER":
			continue

		case "NICK":
			if !wordScanner.Scan() {
				conn.Write([]byte("ERROR: NO NICKNAME\r\n"))
				return nil, false
			}
			nickname := wordScanner.Text()
			ircUser, ok := NewIRCUser(conn, nickname)
			if !ok {
				conn.Write([]byte(ERR_NICKNAMEINUSE("anonymous", nickname)))
				continue
			}
			failure = false
			return ircUser, true

		default:
			conn.Write([]byte("ERROR: Handshake failure\r\n"))
			return nil, false
		}
	}
}

func handshakeCleanup(conn net.Conn, doCleanup *bool) {
	conn.SetReadDeadline(time.Time{})
	if *doCleanup {
		conn.Close()
	}
}
