package mcxircd



import (
	"regexp"
	"strings"
	"sync"
)



var (
	validChannelNameRegexp	string								= "^[#+].*$"
	validChannelNames		*regexp.Regexp						= regexp.MustCompile(validChannelNameRegexp)
	usersInChannels			map[string]map[string]*ircChannel	= make(map[string]map[string]*ircChannel)
	usersInChannelsMutex	*sync.RWMutex						= new(sync.RWMutex)
	privmsgRegexpStr		string								= `^:` + validUsername + `!` + validUsername + `@.* PRIVMSG [#+].* :`
	privmsgRegexp			*regexp.Regexp 						= regexp.MustCompile(privmsgRegexpStr)
)



// Lists all bans for this channel, to user with that nickname
func (myself *ircChannel) makeBanListReply(nickname string) string {
	msg := ""
	for banStr,_ := range myself.banned {
		msg += RPL_BANLIST(nickname, myself.name, banStr)
	}
	msg += RPL_ENDOFBANLIST(nickname, myself.name)
	return msg
}



// Creates a regular expression that is equivalent to a ban-string, if one 
// ignores the username and host (which is not used in MCXHUB).
func makeBanRegexp(banString string) (*regexp.Regexp, bool) {
	n := strings.SplitN(banString, "!", 2)
	if len(n) != 2 { 
		return nil, false
	}
	NickBanStr := n[0]
	validBanStr := `^[a-zA-Z0-9\-\[\]\\_\^\{\}\*` + "`" + `]+$`
	matched,err := regexp.MatchString(validBanStr, NickBanStr)
	if !matched {
		return nil, false
	}
	if err!=nil {
		return nil, false
	}
	safeStr := regexp.QuoteMeta(NickBanStr)
	rStr := strings.Replace(safeStr, `\*`, `.*`, -1)
	r,err := regexp.Compile(rStr)
	if err!=nil {
		return nil, false
	}
	return r, true
}



// checks if a nickname is banned from the channel
func (myself *ircChannel) isBanned(nickname string) bool {
	for _,r := range myself.banned {
		if r.MatchString(nickname) {
			return true
		}
	}
	return false
}



// returns true if user with that nickname has op
func (myself *ircChannel) hasOp(nickname string) bool {
	_, hasOp := myself.operators[nickname]
	return hasOp
}



// returns the mode of the channel
func (myself *ircChannel) modeString() string {
	str := ""
	if myself.linkAnon {
		str += "A"
	}
	if myself.channelKey != "" {
		str += "k"
	}
	if myself.noExternalMessages {
		str += "n"
	}
	if myself.topicLock {
		str += "t"
	}
	return str
}



// returns the name of the twin channel
func (myself *ircChannel) twinChannelName() string {
	if myself.channelType() == '#' {
		return "+" + myself.name[1:]
	} else if myself.channelType() == '+' {
		return "#" + myself.name[1:]
	} else {
		panic("twinChannelName() called for unknown type of channel!")
	}
}



// tries to attach to the twin channel (MODE +A)
func (myself *ircChannel) attachToTwinChannel() bool {
	if myself.twinChannel != nil {
		return true
	}
	cLookup := make(chan namedIRCItem)
	NameSpace.Lookup <- &NSLookupRequest{Key: myself.twinChannelName(), ReplyTo: cLookup}
	switch ircChan := (<-cLookup).(type) {
	case *ircChannel :
		if ircChan.RefAttach() {
			defer ircChan.RefDeattach()
			r := make(chan *chanOPMsg)
			ircChan.chanOP <- &chanOPMsg{From: myself, Command: "TWIN_ATTACH", ReplyTo: r}
			if (<-r).Command == "OK" {
				myself.twinChannel = ircChan
				return true
			} else {
				return false
			}
		} 
	}
	return false
}



// deattaches from the twin channel (MODE -A, or when one of the channel dies), returns true if successful
func (myself *ircChannel) deattachFromTwinChannel() bool {
	if myself.twinChannel == nil {
		return true
	}
	if !myself.twinChannel.RefAttach() {
		return true // twin channel is dying, so it will send us a TWIN_DEATTACH soon, or it already has
	}
	defer myself.twinChannel.RefDeattach()
	r := make(chan *chanOPMsg)
	myself.twinChannel.chanOP <- &chanOPMsg{From: myself, Command: "TWIN_DEATTACH", ReplyTo: r}
	reply := <-r
	if reply.Command == "OK" {
		myself.twinChannel = nil
		return true
	} else {
		return false
	}
}



// returns the symbol representing the type of the channel (ie '#' or '+')
func (myself *ircChannel) channelType() rune {
	return rune(myself.name[0])
}
