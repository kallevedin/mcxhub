// IRC server
package mcxircd

import (
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"log"
	"net"
	"strconv"
)

const (
	TIMEOUT_HANDSHAKE = 60
	TIMEOUT_PING      = 5 * 60
	TIMEOUT_PONG      = 3 * 60
)

func startListen(address string, port int) {
	log.Printf("MCXIRCD: IRC server listening at %s:%d", address, port)
	tcpAddr, e := net.ResolveTCPAddr("tcp", address+":"+strconv.Itoa(port))
	if e != nil {
		log.Printf("MCXIRCD: Could not listen on address:port \"%s:%d\" because: %s!\n", address, port, e.Error())
		return
	}
	listener, e := net.ListenTCP("tcp", tcpAddr)
	if e != nil {
		log.Printf("MCXIRCD: Could not listen on port %d because %s\nMCXIRCD shutting down.", port, e.Error())
		return
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("MCXIRCD: Could not accept on port %d because %s\nMCXIRCD has stopped listening on \"%s:%d\"", address, port, e.Error())
			return
		}
		go Handshake(conn)
	}
}

// Creates a new IRC server. Assumes the presence of a mcxconfig that has been
// initiated.
func StartIRCServer() {
	NameSpace = *makeNameSpace()
	for _,addr := range mcxconfig.GetListenOn() {
		go startListen(addr, mcxconfig.GetIRCPort())
	}
}
