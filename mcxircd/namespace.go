package mcxircd

/*
 * Namespace is a singleton object that has a map[string]namedIRCItem, that
 * contains all named items in the known IRC-world. That is, users and channels.
 * Whenever anything needs to send a message to something else, and it does not
 * have a direct pointer to use, it looks it up using the namespace.
 *
 * Maybe this would be more effective with a mutex. Reads happen a lot more
 * often, and multiple reads can happen simultaneously.
 *
 */
var NameSpace NameSpaceStruct

// Anything that have a name and can be addressed in IRC implements this iterface.
type namedIRCItem interface {
	// The name of the object.
	Identifier() string

	// Sends a message to the object. What this means depends on what object it is.
	SendMessage(from string, message []byte) bool

	// increases the reference counter on the object. It will not die if it has a
	// counter > 0. This is to avoid it dying when you expect it to return your
	// message. You NEED to refDeattach() after calling this one!
	RefAttach() bool
	// Decreases the reference counter. The inverse of refAttach()!
	RefDeattach()
}

type NSLookupRequest struct {
	Key     string
	ReplyTo chan (namedIRCItem)
}

type NSAddRequest struct {
	Key     string
	Value   namedIRCItem
	ReplyTo chan (bool)
}

type NSDelRequest struct {
	Key     string
	ReplyTo chan (bool)
}

type NSReplaceRequest struct {
	Key     string
	NewKey  string
	ReplyTo chan (bool)
}

type NameSpaceStruct struct {
	Lookup  chan (*NSLookupRequest)
	Add     chan (*NSAddRequest)
	Del     chan (*NSDelRequest)
	Replace chan (*NSReplaceRequest)
	Map     map[string]namedIRCItem
}

// Never returns
func (ns *NameSpaceStruct) namespaceWorker() {
	for {
		select {
		case req := <-ns.Lookup:
			r := ns.Map[req.Key]
			req.ReplyTo <- r
		case req := <-ns.Add:
			if ns.Map[req.Key] != nil {
				req.ReplyTo <- false
			} else {
				ns.Map[req.Key] = req.Value
				req.ReplyTo <- true
			}
		case req := <-ns.Del:
			if ns.Map[req.Key] == nil {
				req.ReplyTo <- false
			} else {
				delete(ns.Map, req.Key)
				req.ReplyTo <- true
			}
		case req := <-ns.Replace:
			if value, ok := ns.Map[req.Key]; !ok {
				req.ReplyTo <- false
			} else {
				delete(ns.Map, req.Key)
				ns.Map[req.NewKey] = value
				req.ReplyTo <- true
			}
		}
	}
}

func makeNameSpace() *NameSpaceStruct {
	t := NameSpaceStruct{
		Lookup:  make(chan (*NSLookupRequest)),
		Add:     make(chan (*NSAddRequest)),
		Del:     make(chan (*NSDelRequest)),
		Replace: make(chan (*NSReplaceRequest)),
		Map:     make(map[string]namedIRCItem)}
	go t.namespaceWorker()
	return &t
}
