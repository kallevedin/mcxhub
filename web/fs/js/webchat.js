/*
    Javascript IRC client using framed websocket
    can only be run as a singleton

    Public domain

    Author: Kalle Vedin
    kalle.vedin@fripost.org
    
*/
var ircClient = {
	that: ircClient,
	socketURL: "ws://"+document.location.host+"/api/ircsocket",
	ws: undefined,							// websocket that connects us to IRC server
	rawterm: undefined,						// link to raw IRC terminal
	connected: false,
	clientSendsPINGs: false,				// setting this to true will for example avoid websocket proxies to time out our connection due to inactivity.
	clientPING_intervall: 40,				// number of seconds between pings sent by client; iff clientSendsPINGs = true
	nickname: "bengt",						// nickname to use when logging into IRC server
	username: "webchat",					// username to use when logging into IRC server
	channelsToJoin: ["#telecomix"],			// list of channels to join

	consoles: {},					// list of ircConsole-objects, one for each channel that is joined
	htmlName2ConsoleName: {},		// list of b64(channelname) to channelname
	
	activeConsole: "rawterm",		// currently active window-console, the target of whatever the user enters into the input-box
	
	ircNickRegExp: /[a-zA-Z]{1}[a-zA-Z0-9\[\]\\`_\^\{\|\}]*/			// matches a nickname, iff converted to a regexp
}

// websocket has opened, and we are connected!
ircClient.onOpen = function() {
	that = ircClient;
	that.connected = true;
	that.rawterm.addMessage('<strong>CONNECTED</strong>');
	that.runInitScript(that);
}

// When the server sends a message over websocket, it first enters this function
ircClient.onMessage = function(evt) {
	that = ircClient;
	var msg = evt.data.trimLeft();

	that.rawterm.addMessage(that.IRCtoHTML(evt.data));
	
	// run parser/reaction functions on incomming message
	// TODO: run this until one of them match
	that.handlePing(evt.data);
	that.handleJoin(evt.data);
	that.handlePart(evt.data);
	that.handlePrivmsg(evt.data);
	that.handleNoSuchNickOrChannel(evt.data);
	that.handleNames(evt.data);
	that.handleNamesEnd(evt.data);
	that.handleNickChange(evt.data);
	that.handleKick(evt.data);
}

ircClient.onClose = function(event) {
	that = ircClient;
	that.connected = false;
	that.rawterm.showWindow();
	that.rawterm.addMessage('<strong>DISCONNECTED</strong>');
	that.rawterm.addMessage('Will restart your client in 10 seconds.', "CLIENT NOTICE");
	window.setTimeout(ircClient.restart, 10000);
}

ircClient.restart = function() {
	window.location.reload();
}

ircClient.onError = function(event) {
	that = ircClient;
	that.connected = false;
}

// transfers focus to the input field at the bottom
ircClient.focusOnInput = function() {
	$("#userinput")[0].focus();
}

// Encodes any binary into a string that is valid as an HTML ID name. (for example, updateRawConsole('<div id="' + HTMLb64encode(binaryData) + '"/>');)
ircClient.HTMLb64encode = function (text) {
	return btoa(text).replace(/\//g, ":").replace(/=/g, "_");
}

// Decodes an HTMLb64encoded string into a binary
ircClient.HTMLb64decode = function(text) {
	return atob(text.replace(/_/g, "=").replace(/:/g, "\/"));
}

// Escapes a string, so that its symbols can not be interpreted as commands in for example regular expressions
ircClient.escape = function(text) {
	return text.replace(/\[/g, "\\\[").replace(/\]/g, "\\\]").replace(/\\/g, "\\\\").replace(/\{/g, "\\\{").replace(/\}/g, "\\\}");
}

// Creates a timestamp of type hh:mm:ss, example 19:01:14
ircClient.timestamp = function() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    str += hours + ":" + minutes + ":" + seconds + " ";
    return str;
}

// Sends a command to the IRC server or notifies the user that there was an error
ircClient.sendLn = function(msg) {
	try {
		ws.send(msg + "\r\n");
	} catch (e) {
		this.rawterm.addMessage('<strong>Client: Unable to send message "' + this.IRCtoHTML(msg) + '"</strong>');
	}
}

// replies to PINGs from the server, thus avoiding timeout and disconnect
ircClient.handlePing = function(msg) {

	var m = msg.match(/^PING(.*)$/);
	if (m !== null) {
		var reply = "";
		if( m[1] !== "" ) {
			reply = "PONG :" + m[1];
		} else {
			reply = "PONG";
		}
		this.sendLn(reply)
		this.rawterm.addMessage(this.IRCtoHTML(reply), "CLIENT");
	} else if (msg.trimRight() == "PING") {
		this.sendLn("PONG");
		this.rawterm.addMessage("PONG", "CLIENT");
	}
}

// detects if the server told us that we joined a channel, and if so, creates a new terminal for the channel.
ircClient.handleJoin = function(msg) {
	// Server sends ":urban!urban@cloak JOIN #mcx"

	var m = msg.match(/^:([^ ]+)![^ ]+@[^ ]+ JOIN (.+)$/);
	if (m !== null) {
		var nickname = m[1],
		    chanName = m[2];

		if (nickname === this.nickname) {
			// we joined a channel - create new window and tab for that channel!
			var channel = ircConsole.create(this, chanName, "channel");
			channel.showWindow();
			channel.addMessage("You joined channel " + this.IRCtoHTML(channel.name));
		} else {
			// someone else have joined a channel we are in - add it to the list of users in that channel.
			if(chanName !== null) {
				if(this.consoles[chanName] === undefined) {
					// someone joined a channel we are inside, but are unaware that we have joined (this can happen!).
					// So create the channel.
					ircConsole.create(this, chanName, "channel");
				}
				this.consoles[chanName].nickJoined(nickname);
			}
		}
		return true;
	} else {
		return false;
	}
}

/**
 * 
 */
ircClient.handlePart = function(msg) {
	// Server sends ":bengt!bengt@cloak PART #chan :optional part message"
	
	var m = msg.match(/^:([^ ]+)![^ ]+@[^ ]+ PART (.*) :.*$/);
	if (m !== null) {
		var nickname = m[1],
		    chanName = m[2];

		if (nickname === this.nickname) {
			// we left a channel... 
			if (chanName !== null) {
				if(this.consoles[chanName] !== undefined) {
					this.consoles[chanName].destroy();
				}
			}
		} else {
			// someone else left a channel...
			channel = this.consoles[chanName];
			if( channel !== undefined ) {
				channel.nickLeft(nickname);
			}
		}
		return true;
	} else {
		return false;
	}
}

// inserts messages received from server into correct chat room
ircClient.handlePrivmsg = function(msg) {
	// :nick!user@hostname PRIVMSG # :hej bengt		<-- example syntax of privmsg from server

	var m = msg.match(/^:([^ ]+)![^ ]+@[^ ]+ (PRIVMSG|NOTICE|QUERY) ([^ ]+) :(.*)$/);
	if (m !== null) {
		var sender = m[1],		//< nickname of who sent the message
		    msgType = m[2];		//< type of message (usually PRIVMSG)
		    destination = m[3],		//< receiver of message
		    message = m[4];		//< the message sent
		if( sender !== null && destination !== null && message !== null ) {
			if( destination === this.nickname ) {
				// it is a direct private message to us
				if (this.consoles[sender] === undefined ) {
					// we don't have a private conversation console with this user -- so we create it
					this.consoles[sender] = ircConsole.create(this, sender).flash();
				}
				this.consoles[sender].addMessage(message, sender).flash();
				
			} else if ( this.consoles[destination] !== null ) {
				// its a message directed to a channel we joined
				this.consoles[destination].addMessage(message, sender).flash();
			} else {
				// if this happens, we probably got a message for a channel that we believe we have not joined, but have
				// nontheless actually joined.
				// It probably is a good idea to ignore this type of messages.
				
				// TODO comment out the alert below in production code 
				alert("got message from: " + sender + "\n" +
				      "message type: " + msgType + "\n" +
				      "directed to: " + destination + "\n" +
				      "message was: " + message + "\n");
			}
		}
		return true;
	}
	return false;
}

/**
 * Detects if a nickname that the user is trying to chat with have disconnected, and displays a message in the chat console.
 * ACTUALLY: This method captures messages sent from the server, which says that something we tried to access (user or channel) does not exist.
 */
ircClient.handleNoSuchNickOrChannel = function(msg) {
	// :server.domain.name 401 clientNick unavailableName :No such nick/channel	<-- example syntax that server sends to notify clientNick that unavailableName does not exist.
	
	var m = msg.match(/^:[^ ]+ 401 ([^ ]+) ([^ ]+) :.*$/);
	if( m !== null ) {
		var mynick = m[1],
		    unavailableName = m[2];
		if( this.consoles[unavailableName] !== undefined ) {
			var htmlName = this.IRCtoHTML(unavailableName);
			this.consoles[unavailableName].addMessage(htmlName + " have disappeared. You may close this window, or wait if you believe " + htmlName + " will come back online again.");
		}
	}
}

/**
 * This function populates the userlist for channels.
 * Captures 353 and 366 numeric messages from the IRC server and translates them into a list of users for a channel.
 * This function works together with handleNamesEnd()!
 * See RFC2812 §3.2.1 and §3.2.5 for details.
 */
ircClient.handleNames = function(msg) {
	// :server.domain.name 353 clientNick = channelName :bengt @kaos
	// :server.domain.name 366 clientNick channelName :End of NAMES list
	//
	// some/most IRC servers limit their replies to 512 bytes. This means that if there are really many users in a channel,
	// the 353-response might be split into multiple responses. We therefor collect the names dropped by the server into a
	// temporary variable (userListNew), and then handleNamesEnd() handles the rest.

	var m = msg.match(/^:[^ ]+ 353 [^ ]+ [=\*] ([^ ]*) :(.*)$/);
	if( m !== null ) {
		var chanName = m[1],
		    userList = m[2],
		    channel = this.consoles[chanName];

		if( channel !== undefined && channel.consoleType === "channel" ) {
			if( channel.userListNew === undefined ) {
				channel.userListNew = [];
			}
			channel.userListNew = channel.userListNew.concat(userList.trim().split(/ +/));
		}
	}
}

ircClient.handleNamesEnd = function(msg) {
	// :fake.name 366 bengt # :End of NAMES list

	var m = msg.match(/^:[^ ]+ 366 [^ ]+ ([^ ]*) :(.*)$/);
	if( m !== null ) {
		var chanName = m[1],
		    channel = this.consoles[chanName];

		if ( channel !== undefined && channel.consoleType === "channel" ) {
			channel.updateUserList();
		}
	}
}

/**
 * Detects if someone changes their nickname, and notifies all relevant consoles about it so that they may 
 * update their GUIs.
 */
ircClient.handleNickChange = function(msg) {
	// :oldNick!username@domain.tld NICK newNick   <-- sent from server whenever a user change nick

	var m = msg.match(/^:([^ ]+)![^ ]+ NICK ([^ ]*)$/);
	if( m !== null ) {
		var oldNick = m[1],
		    newNick = m[2],
		    privConsole = this.consoles[oldNick];

		if( privConsole !== undefined ) {
			privConsole.setName(newNick);
		}
		if ( oldNick === this.nickname ) {
			this.nickname = newNick;
		}
		for(var cn in this.consoles) {
			cons = this.consoles[cn];
			if( cons.consoleType === "channel" ) {
				cons.nickChange(oldNick, newNick);
			}
		}
	}
}

ircClient.handleKick = function(msg) {
	// :nick!user@domain KICK chanName whoIsKicked :reason

	var m = msg.match(/^:([^ ]+)![^ ]+@[^ ]+ KICK ([^ ]+) ([^ ]+) :(.*)$/);
	if( m !== null ) {
		var kicker = m[1],
		    chanName = m[2],
		    kicked = m[3],
		    reason = m[4];

		var channel = this.consoles[chanName];
		if( channel !== undefined ) {
			var because = ( reason === "" ? "" : ", because \"" + reason + "\"");

			if( kicked === this.nickname ) {
				// you were kicked
				this.rawterm.addMessage("You were kicked off " + chanName + " by " + kicker + because, "YOU WERE KICKED");
				channel.destroy();
			} else {
				// someone else was kicked
				channel.nickLeft(kicked, "kicked by " + kicker + because);
			}
		}
	}
}

/**
 * Whenever the user hits a key while having the input field marked, this method runs.
 * 
 * Keys    : Function
 * Return  : Send message to the active window and clear the input field
 * Up/Down : Iterates through the input history (TODO)
 */
ircClient.keyPressed = function(event) {
	event = event || window.event;
	
	that = ircClient;
	// user hits return
	if (event.keyCode == "13") {
		if(!that.connected) {
			ircClient.rawterm.addMessage('Can not send command to server: You are not connected to it.');
		} else {
			var text = $("#userinput").val();
			if (text !== "") {
				// interpret /j #channel and /part #channel as commands, and not anything the user wants to write
				var m
				if ((m = (text.trim().match(/^\/(j|join) ([^ ]+)$/))) !== null ){
					var chanName = m[2];
					if (that.consoles[chanName] !== undefined) {
						that.consoles[chanName].showWindow();
					} else {
						var command = "JOIN " + chanName;
						that.consoles["rawterm"].addMessage(command, "CLIENT");
						that.sendLn(command);
					}
				} else if ((m = (text.trim().match(/^\/part ([^ ]+)$/))) !== null ) {
					var chanName = m[1];
					if (that.consoles[chanName] !== undefined) {
						that.consoles[chanName].destroy();
					} else {
						// send the message anyway, maybe the user knows best?
						var command = "PART " + chanName;
						that.consoles["rawterm"].addMessage(command, "CLIENT");
						that.sendLn(command);
					}
				} else if ((m = (text.trim().match(/^\/part$/))) !== null ) {
					if (that.activeConsole !== "rawterm") {
						that.consoles[that.activeConsole].destroy();
					} else {
						that.consoles["rawterm"].addMessage("It is a somewhat bad idea to destroy the rawterm", "CLIENT");
					}
				} else {
					that.consoles[that.activeConsole].sendToConsole(text);
				}
			}
		}
		$("#userinput").val("");
	}
	that.focusOnInput();
}

// This function is run every 40 sec to avoid websocket proxies to timeout our connection. Default usually is to
// close a connection after 60 seconds if nothing has been sent.
ircClient.sendPingToServer = function sendPingToServer() {
	var msg = "PING :keepalive";
	this.rawterm.addMessage(msg, "CLIENT");
	this.sendLn(msg);
}

// Makes IRC-commands/replies/messages HTML-printable
ircClient.IRCtoHTML = function(str) {
	// avoid interpreting IRC messages as HTML
	str = str.replace(/</g, "&lt;").replace(/>/g, "&gt;")
	
	// accept multispace
	str = str.replace(/ /g, "&nbsp;");
	
	// remove colors
	str = str.replace(/\x03[0-9]{0,2}(,[0-9]{1,2})?/g, "");
	
	// remove bold text
	str = str.replace(/\x02/g, "");
	
	// linkify all URLs
	urlify = function(match){
		return '<a class="irclink" href="' + match + '">' + match + '</a>';
	}
	str = str.replace(/(http|https|ftp|sftp|fish|ssh|ws|wss|irc):\/\/([:_=+%&!a-zA-Z0-9\.\?\-\/]){4,}/g, urlify);

	return str;
}

ircClient.privChat = function(nick) {
	nick = this.HTMLb64decode(nick);
	if( this.consoles[nick] !== undefined ) {
		this.consoles[nick].showWindow();
	} else {
		this.consoles[nick] = ircConsole.create(this, nick).showWindow();
	}
	this.focusOnInput();
}

// Runs a very short list of commands. Should preferably be called after we have a websocket connection.
ircClient.runInitScript = function(that) {
	var script = ["USER $user 0 * :$user", "NICK $nick"]; // the script to run
	for (var i in script) {
		script[i] = script[i].replace(/\$user/g, that.username);
		script[i] = script[i].replace(/\$nick/g, that.nickname);
	}
	for (var i in that.channelsToJoin) {
		if( that.channelsToJoin[i] !== "") {
			script = script.concat(["JOIN " + that.channelsToJoin[i].trim()]);
		}
	}
	script = script.concat(["HELP"]); // make server show some help, in case the user pokes around in the raw terminal
	this.rawterm.addMessage('<strong>Initscript start</strong>');
	for (var i in script) {
		this.rawterm.addMessage(that.IRCtoHTML(script[i]));
		that.sendLn(script[i]);
	}
	this.rawterm.addMessage('<strong>Initscript complete</strong>');
}

/**
 * Creates a new IRC client:
 *    - creates a raw IRC console
 *    - connects to the websocket
 *    - hooks us up with event handlers
 */
ircClient.initClient = function() {
	var rawterm = ircConsole.create(ircClient, "rawterm");
	rawterm.sendToConsole = function(message) {
		this.addMessage(message, "You");
		this.owner.sendLn(message);
	}
	rawterm.showWindow();
	
	if (!("WebSocket" in window)) {
		rawterm.addMessage('<p class="alert">Your web browser does not support websocket.</p>');
		rawterm.addMessage('<p>Maybe you should update your browser?</p>');
		return false;
	}
	that = ircClient;
	that.rawterm = rawterm;
	
	// connect to the server
	ws = new WebSocket(that.socketURL);
	ws.binaryType = "arraybuffer";
	ws.onopen = that.onOpen;
	ws.onmessage = that.onMessage;
	ws.onclose = that.onClose;
	ws.onerror = that.onError;
	that.ws = ws;

	$("#userinput").keypress(that.keyPressed);
	
	// optinal: avoid websocket proxies to timeout our connection
	if(that.clientSendsPINGs) {
		setInterval( "ircClient.sendPingToServer()", ircClient.clientPING_intervall * 1000 );
	}
	this.focusOnInput();
	return true
}


// #####################################################################################################################################################

// IRC Channel-object, represents an IRC channel.
var ircConsole = {
	owner: undefined,			// ircClient that this channel belongs to.
	name: undefined,			// console name.
	consoleType: undefined,		// "raw", "channel", "user"
	htmlName: undefined,		// the name of the console, inside the HTML tags.
}

// creates a new ircConsole
ircConsole.create = function(owner, name, consoleType) {
	var F = function() {};
	F.prototype = ircConsole;
	var that = new F();
	
	that.owner = owner;
	that.consoleType = consoleType;

	ircConsole.setName.apply(that, [name]);
	that.owner.focusOnInput();
	return that;
}

/**
 * Sets the name of the console and makes various datastructures point to it. 
 * Also updates all references to the console everywhere, in case it already had a name.
 */ 
ircConsole.setName = function(name) {
	consoleAlreadyExisted = ( this.name !== undefined );

	var oldName, oldTabID, oldWinID;

	if( consoleAlreadyExisted ) {
		delete this.owner.htmlName2ConsoleName[this.htmlName];
		delete this.owner.consoles[this.name];
		oldName = this.name;
		oldTabID = this.tabID;
		oldWinID = this.winID;
	}

	this.name = name;
	this.htmlName = this.owner.HTMLb64encode(name);
	this.owner.htmlName2ConsoleName[this.htmlName] = this;
	this.owner.consoles[this.name] = this;
	this.tabID = this.htmlName + '-tab';
	this.winID = this.htmlName + '-window';
	if( this.consoleType === "channel" ) {
		this.chanID = this.htmlName + '-channel';
		this.listID = this.htmlName + '-userlist';
	}
	if( !consoleAlreadyExisted ) {
		if(this.name === "rawterm") {
			// rawterm have no close button
			$("#windowselector").append('<span id="' + this.tabID + '" class="windowtab"><span onClick="ircClient.htmlName2ConsoleName[\'' + this.htmlName + '\'].showWindow()">' + this.name + '</span></span>');	
		} else {
			// all other windows has one
			$("#windowselector").append('<span id="' + this.tabID + '" class="windowtab"><span class="tab-text" onClick="ircClient.htmlName2ConsoleName[\'' + this.htmlName + '\'].showWindow()">' + this.name + '</span><span><img onClick="ircClient.htmlName2ConsoleName[\'' + this.htmlName + '\'].destroy()" src="/fs/x-14.png" class="x-button" /></span></span>');
		}
		$("#terminalContainer").append('<div id="' + this.winID + '" class="hidden"></div>');
		if( this.consoleType === "channel" ) {
			$("#" + this.winID).append('<div id="' + this.chanID + '" class="channel"></div>');
			$("#" + this.winID).append('<div id="' + this.listID + '" class="userlist"></div>');
		}
	}
	if( consoleAlreadyExisted ) {
		$("#" + oldTabID).replaceWith('<span id="' + this.tabID + '" class="windowtab"><span class="tab-text" onClick="ircClient.htmlName2ConsoleName[\'' + this.htmlName + '\'].showWindow()">' + this.name + '</span><span><img onClick="ircClient.htmlName2ConsoleName[\'' + this.htmlName + '\'].destroy()" src="/fs/x-14.png" class="x-button" /></span></span>');
		$("#" + oldWinID).attr('id', this.winID);
		this.addMessage(this.owner.IRCtoHTML(oldName) + " changed name to " + this.owner.IRCtoHTML(this.name));
	}
	if( this.owner.activeConsole === oldName ) {
		this.owner.activeConsole = this.name;
	}
	return this;
}

/**
 * Destroys the console. Removes all references to it.
 */
ircConsole.destroy = function() {
	if( this.name === "rawterm" ) {
		this.owner.restart();
	}
	if( this.owner.activeConsole === this.name ) {
		this.owner.consoles["rawterm"].showWindow();
	}
	$("#" + this.tabID).remove();
	$("#" + this.winID).remove();
	delete this.owner.htmlName2ConsoleName[this.htmlName];
	delete this.owner.consoles[this.name];

	// if someone destroys a channel, for example by clicking on the X, we need to tell the server
	// that we are leaving the channel.
	if( this.consoleType === "channel" ) {
		var ircCommand = "PART " + this.name;
		this.owner.rawterm.addMessage(ircCommand, "CLIENT");
		this.owner.sendLn(ircCommand);
	}
}

// Makes the channel window visible (and makes all other windows hidden)
ircConsole.showWindow = function() {
	$("#" + this.owner.consoles[this.owner.activeConsole].winID)[0].className = "hidden";
	$("#" + this.owner.consoles[this.owner.activeConsole].tabID)[0].className = "windowtab";
	if( this.consoleType === "channel" ) {
		$("#" + this.winID)[0].className = "channelConsole";
	} else {
		$("#" + this.winID)[0].className = "visibleConsole";
	}
	$("#" + this.tabID)[0].className = "windowtab selected";	
	this.owner.activeConsole = this.name;
	this.owner.focusOnInput();
	this.scrollToBottom();

	return this;
}

// scrolls to bottom of text/chat field
ircConsole.scrollToBottom = function() {
	if( this.consoleType === "channel" ) {
		id = this.chanID;
	} else {
		id = this.winID;
	}
	var d = document.getElementById(id);
	d.scrollTop = d.scrollHeight;
}

// flashes the tab
ircConsole.flash = function() {
	if( this.owner.activeConsole !== this.name ) {
		$("#" + this.tabID)[0].className = "windowtab attention";
	}

	return this;
}

// Adds a message with an optional sender to the console
ircConsole.addMessage = function(message, from) {
	if(from) {
		if( from === this.owner.nickname ) {
			message = '<span class="user-highlight">You:</span>&nbsp;' + this.owner.IRCtoHTML(message);
		} else {
			message = '<span class="other-highlight">' + this.owner.IRCtoHTML(from) + ':</span>&nbsp;' + this.owner.IRCtoHTML(message);
		}
	}
	this.insertHTML('<p class="irc"><span class="timestamp">' + this.owner.timestamp() + '</span>&nbsp;' + message + '</p>');
	
	return this;
}

// Inserts a message into the channel window.
// Message should be an HTML tag.
ircConsole.insertHTML = function(text) {
	// TODO also make the tab flash, if this is not the active console
	
	var id;
	if( this.consoleType === "channel" ) {
		id = this.chanID;
	} else {
		id = this.winID;
	}

	var d = document.getElementById(id);
	var isScrolled = d.offsetHeight == d.scrollHeight - d.scrollTop;
	d.innerHTML += text;
	if( isScrolled ) {
		d.scrollTop = d.scrollHeight;
	}
	
	return this;
}

// Sends a message to the console (replace this function for weird consoles, like the raw terminal (rawterm))
ircConsole.sendToConsole = function(message) {
	var ircCommand = "PRIVMSG " + this.name + " :" + message;
	this.addMessage(message, this.owner.nickname);
	this.owner.rawterm.addMessage(ircCommand, "CLIENT");
	this.owner.sendLn(ircCommand);
}




ircConsole.updateUserList = function() {
	if( this.consoleType !== "channel" ) {
		alert("DERP");
	}
	this.userList = this.userListNew.map(this.separateChanModFromNick);
	delete this.userListNew;
	this.redrawUserList();
}

ircConsole.redrawUserList = function() {
	var newList = "";
	this.userList = this.userList.sort();
	for(var i=0; i!=this.userList.length; i++) {
		var nick = this.userList[i][0],
		    id = this.makeNickListID(nick),
		    //htmlNick = this.owner.IRCtoHTML(this.userList[i][1] + nick);		prints @ when users are oped
		    htmlNick = this.owner.IRCtoHTML(nick);

			newList += '<div id="' + id + '" onClick="ircClient.privChat(\'' + this.owner.HTMLb64encode(nick) + '\')" class="userInUserlist">' + htmlNick + '</div>\n';
	}
	$("#" + this.listID).empty().append(newList);
}

ircConsole.separateChanModFromNick = function(nick) {
	var m = nick.match(/^([@\+&]+)(.*)$/);
	if( m !== null ) {
		return [m[2], m[1]];
	} else {
		return [nick, ""];
	}
	return obj;
}

ircConsole.joinChanModWithNick = function(arg) {
	nick = arg[0];
	chanmod = arg[1];
	// we only care about op here, because i dont want to write this code for all of eternity
	if( chanmod.indexOf("@") !== -1 ) {
		return "@" + nick;
	} else {
		return nick;
	}
}

ircConsole.makeNickListID = function(nick) {
	return this.owner.HTMLb64encode( nick + '@' + this.name );
}

ircConsole.nickChange = function(oldNick, newNick) {
	var who = ( newNick === this.owner.nickname ? "<b>You</b>" : this.owner.IRCtoHTML(oldNick) );
	this.addMessage(who + " changed nickname to " + this.owner.IRCtoHTML(newNick));
	for(var i=0; i!=this.userList.length; i++) {;
		if(this.userList[i][0] === oldNick) {
			this.userList[i][0] = newNick;
			/*
			console.log( $("#" + this.makeNickListID(oldNick)).attr("id", this.makeNickListID(newNick)).attr("onClick", 
				"ircClient.privChat('" + this.owner.HTMLb64encode(newNick) + "')").html(this.joinChanModWithNick(this.userList[i])));
			*/
			this.redrawUserList();
			return this;
		}
	}
	return this;
}

/**
 * adds a nickname to the channel userlist.
 */
ircConsole.nickJoined = function(nickname) {
	this.addMessage(nickname + " joined");
	for(var i=0; i!=this.userList.length; i++) {
		if( this.userList[i][0] === nickname ) {
			return;
		}
	}
	this.userList.push([nickname, ""]);
	this.userList = this.userList.sort(function(a, b){ return (a[0] > b[0]); });
	this.redrawUserList();
}

/**
 * removes a nickname from the channel userlist.
 */
ircConsole.nickLeft = function(nickname, how) {
	msg = nickname + " left";
	if( how !== null && how !== undefined ) {
		msg += " -- " + how;
	}
	this.addMessage(msg);
	var b = false;
	this.userList = this.userList.filter(function(v){
		if(b) {
			return true;
		}
		if(v[0] === nickname) {
			b = true;
			return false;
		}
		return true;
	});
	this.redrawUserList();
}




function itWorks() {
	$("#welcome").empty().append('<input type="text" name="nickname" id="nicknameOpt" class="ircOption" type="text" pattern="^[a-zA-Z]{1}[a-zA-Z0-9\\[\\]\\\\`_\\^\\{\\|\\}]*$" placeholder="your nickname" autofocus /><br />' +
			'<input type="text" name="channels" id="channelsOpt" class="ircOption" type="text" pattern="^[#\+][^ ]*$" placeholder="#channel" /><br />' +
			'<p class="ircOption">Nicknames longer than 8 symbols does not work with all servers. You may specify multiple channels to join by separating their names with comma, space or both.</p>' +
			'<input type="button" name="startChat" id="startChatButton" onClick="ircWrapper()" class="ircOption" value="ENTER" />');
}

function ircWrapper() {
	ircClient.nickname = $("#nicknameOpt").val();
	ircClient.channelsToJoin = $("#channelsOpt").val().split(/[ \,]/g);

	$("#welcome").remove();
	$("#container").append('<div id="windowselector">' +
		'</div>' +
		'<div id="terminalContainer">' +
		'</div>' +
		'<input id="userinput" type="text" autofocus="true" />"' );
	ircClient.initClient();
}
$(document).ready(itWorks);
