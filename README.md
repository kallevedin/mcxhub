MCXHUB
======

Its an IRC server. 

It has a built-in websocket webchat served off a minimalistic web server.

IRC
---
Features and quirks:

* You can log into channels and also chat with others (yey!).
* Basic channel functions you would expect, such as chanops and topic. But no voice. Because fuck voice.
* There are anonymous and regular channels. Anonymous channels have names that begins with + instead of #. Setting mode +A in #-channels merges them.
* Filters and rejects Client To Client-Protocol (CTCP) commands. CTCP can be used to query users about the client software they use, or to trick users into revealing their IP addresses for sending/receiving files.
* Silently translates NOTICE and SQUERY into PRIVMSG. This means that there is no difference between users, bots and services. They are all treated equally - which I think actually makes things easier.
* Accepts at most 20 arguments for all commands. RFC2812 says maximum is 3.
* Somewhat RFC2812-compliant, when its not blatantly violating the standards.
* Very basic spam protection.

Web
---
Features and quirks:

 * Easy to turn the http server off.
 * Cute minimal default frontpage that you can easily replace.
 * Really simple static file sharing (in the directory ./web/fs/)
 * IRC over WebSockets
    + ws://yourserver:webport/api/ircsocket - One IRC message per frame and auto-newlines.
    + ws://yourserver:webport/api/rawircsocket - Transports real, raw IRC, as if it was a TCP-stream.
 * A JavaScript IRC client.

It is quite easy to move the irc client off the web server and just have nginx or whatever proxy the websocket.

Roadmap for the future
----------------------

 * Remove everything except the websocket from the web server
 * Try to make this more minimalist by shaving off features that no one needs

Setup
-----
If you want to run MCXHUB, do this:

* Build it from source (see above)
* Create a directory somewhere. Lets call it D.
* Copy the mcxhub.conf, MOTD and OPERS files from the source and put them in D. Read them and modify them!
* Copy the directory "web" into D. Optionally modify web/index.html. You can do exactly whatever you want with all files in web/fs/, but web/index.html NEEDS to be present.
* Goto D and run mcxhub from inside that directory. It expects a lot of files to be in the current working directory.
* Done! You can now connect to the IRC server at localhost:6667, and the web server listens at localhost:6502. (Unless you changed the ports it should listen to in the mcxhub.conf file.)

License
-------
Simple 2-clause BSD.

Author
------
Kalle Vedin (kalle.vedin@fripost.org)
