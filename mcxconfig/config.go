// Configuration files, passwords, etc
package mcxconfig

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"io/ioutil"
	"strconv"
	"strings"
)

var (
	// default values for the config file, if nothing was specified for some variable.
	defaultconfigMap = map[string]string{
		"fullname":         "herp.derp",
		"shortname":        "mcxhub",
		"software":         "mcxhub",
		"version":          "mcxhub-1",
		"hostcloak":        "cloak",
		"quote":            "Om mani padme hum",
		"defaultrealname":  "Anonymous Pseudonym",
		"maxnicklen":       "9",
		"listen-on":        "127.0.0.1",
		"ircport":          "6667",
		"start-webserver":  "NO",
		"webport":          "6502",
		"use-i2p":          "NO",
		"sam-bridge":       "127.0.0.1:7655",
		"data-dir":         "./data",
		"salt":             "I DID NOT READ THE CONFIG FILE",
	}
	opersMap  map[string]string = nil
	configMap map[string]string = nil
	salt      string

	// The Message Of The Day
	MOTD [][]byte = nil

	// Path to, or the name of the MOTD file (used to remember where it was, if we want to reload its content)
	MOTDFile string = "MOTD"

	// Path to, or the name of the OPERS file (used to remember where it was, if we want to reload its content)
	OPERSFile string = "OPERS"

	// Path to, or the name of the MCXHUB config file (used to remember where it was, if we want to reload its content)
	ConfFile string = "mcxhub.conf"
)

// Reads a config file (mcxhub.conf) and updates the server variables
// accordingly. This should only be done from the REHASH() function, or at
// server initialization. Returns true unless fatal err.
func ReadConfigFile(confFileName string) (string, bool) {
	configFileFound := true
	configMap := make(map[string]string)
	for k, v := range defaultconfigMap {
		configMap[k] = v
	}
	b, e := ioutil.ReadFile(confFileName)
	if e != nil {
		msg := "No config file found. Falling back to default values:\n"
		for key, value := range configMap {
			msg += fmt.Sprintf("\t%s = %s\n", key, value)
		}
		log.Printf("%s", msg)
		configFileFound = false
	} else {
		ConfFile = confFileName // save for future reference (REHASH)
		lineScanner := bufio.NewScanner(strings.NewReader(string(b)))
		for lineNum := 1; lineScanner.Scan(); lineNum++ {
			line := strings.TrimSpace(lineScanner.Text())
			if len(line) == 0 || (len(line) > 0 && line[0] == '#') {
				// ignore empty lines and lines that begin with #
				continue
			}
			keyVal := strings.SplitN(line, "=", 2)
			if len(keyVal) == 2 {
				key := strings.ToLower(strings.TrimSpace(keyVal[0]))
				value := strings.TrimSpace(keyVal[1])
				if value[0] == '"' && value[len(value)-1] == '"' {
					value = value[1 : len(value)-1] // remove dem quotes
				}
				delete(configMap, key)
				configMap[key] = value
			} else {
				return fmt.Sprintf("syntax error in %s line %s", confFileName, lineNum), false
			}
		}
	}
	if v, p := configMap["fullname"]; p {
		SetFullName(v)
	}
	if v, p := configMap["shortname"]; p {
		SetShortName(v)
	}
	if v, p := configMap["software"]; p {
		SetSoftware(v)
	}
	if v, p := configMap["version"]; p {
		SetVersion(v)
	}
	if v, p := configMap["hostcloak"]; p {
		SetHostcloak(v)
	}
	if v, p := configMap["quote"]; p {
		SetQuote(v)
	}
	if v, p := configMap["defaultrealname"]; p {
		SetDefaultRealName(v)
	}
	if v, p := configMap["maxnicklen"]; p {
		if vi, e := strconv.Atoi(v); e != nil {
			return fmt.Sprintf("%s: Value specified for \"maxnicklen\" is not a number: %s", confFileName, v), false
		} else {
			SetMaxNickLen(vi)
		}
	}
	if v, p := configMap["listen-on"]; p {
		SetListenOn(strings.Split(v, ","))
	}
	if v, p := configMap["ircport"]; p {
		if vi, e := strconv.Atoi(v); e != nil {
			return fmt.Sprintf("%s: Value specified for \"ircport\" is not a number: %s", confFileName, v), false
		} else {
			SetIRCPort(vi)
		}
	}
	if v, p := configMap["start-webserver"]; p {
		if b,ok := readTrueOrFalse(v); !ok {
			return fmt.Sprintf("%s: Value specified for \"webserver\" can not be interpreted as a boolean (It needs to be \"yes\"/\"true\" or \"no\"/\"false\"): %s", confFileName, v), false
		} else {
			SetStartWebserver(b)
		}
	}
	if v, p := configMap["webport"]; p {
		if vi, e := strconv.Atoi(v); e != nil {
			return fmt.Sprintf("%s: Value specified for \"webport\" is not a number: %s", confFileName, v), false
		} else {
			SetWebPort(vi)
		}
	}
	if v, p := configMap["use-i2p"]; p {
		if b,ok := readTrueOrFalse(v); !ok {
			return fmt.Sprintf("%s: Value specified for \"use-i2p\" can not be interpreted as a boolean (It needs to be \"yes\"/\"true\" or \"no\"/\"false\"): %s", confFileName, v), false
		} else {
			SetUseI2P(b)
		}
	}
	if v, p := configMap["sam-bridge"]; p {
		SetSAMBridge(v)
	}
	if v, p := configMap["data-dir"]; p {
		SetDataDir(v)
	}
	
	if v, p := configMap["salt"]; p {
		salt = v
	}
	
	if !configFileFound {
		return "NO CONFIG FILE FOUND (Tried using: " + ConfFile + ")", true
	}
	return "SUCCESS", true
}

// determines if a string represents true of false. YES/yes/true = true, and
// NO/no/false = false. Returns _,false if it was impossible to determine the
// boolean meaning.
func readTrueOrFalse(yesno string) (bool, bool) {
	if strings.EqualFold(yesno, "yes") || strings.EqualFold(yesno, "true") {
		return true, true
	} else if strings.EqualFold(yesno, "no") || strings.EqualFold(yesno, "false") {
		return false, true
	}
	return false, false
}


// Reads a Message Of The Day-file (MOTD) and updates the server variables
// accordingly. This should only be done from the REHASH() function, or at
// server initialization. Returns true unless fatal err.
func ReadMOTDFile(motdFileName string) (string, bool) {
	MOTD = nil
	b, e := ioutil.ReadFile(motdFileName)
	if e != nil {
		return fmt.Sprintf("No MOTD file found. Will use default prayer."), false
	}
	MOTDFile = motdFileName // save for future reference (REHASH)
	MOTD = bytes.Split(b, []byte{'\n'})
	return "SUCCESS", true
}

// Reads the OPERS file and updates the server variables accordingly. This
// should only be done from the REHASH() function, or at server initialization.
// Returns true unless fatal err.
func ReadOPERSFile(opersFileName string) (string, bool) {
	opersMap = make(map[string]string)
	b, e := ioutil.ReadFile(opersFileName)
	if e != nil {
		return fmt.Sprintf("No OPERS file found. There will be no system operators for this mcxhub."), false
	}
	OPERSFile = opersFileName // save for future reference (REHASH)
	lineScanner := bufio.NewScanner(strings.NewReader(string(b)))
	for lineNum := 1; lineScanner.Scan(); lineNum++ {
		line := strings.TrimSpace(lineScanner.Text())
		if len(line) == 0 || (len(line) > 0 && line[0] == '#') {
			// ignore empty lines and lines that begin with #
			continue
		}
		keyVal := strings.SplitN(line, " ", 2)
		if len(keyVal) != 2 {
			return fmt.Sprintf("Syntax error in file %s line %d", opersFileName, lineNum), false
		}
		opersMap[keyVal[0]] = keyVal[1]
	}
	return "SUCCESS", true
}

// Reloads *all* configuration files and updates a lot of variables. Typically run as a result from
// a system operator entering a REHASH-command (/REHASH in IRC.) Returns a list of strings containing
// human-readable info about how it went, what happened.
func REHASH() ([]string, bool) {
	confStr, okconf := ReadConfigFile(ConfFile)
	motdStr, okmotd := ReadMOTDFile(MOTDFile)
	opersStr, okopers := ReadOPERSFile(OPERSFile)

	if okconf && okmotd && okopers {
		return []string{"SUCCESS"}, true
	} else {
		infoStrs := []string{"REHASHING: Not everything went all right.", 
			fmt.Sprintf("CONFIG-file: %s", confStr), 
			fmt.Sprintf("MOTD-file: %s", motdStr), 
			fmt.Sprintf("OPERS-file: %s", opersStr)}
		return infoStrs, false
	}
}

// returns true if the password for the specified operator was correct!
func OperLogin(operatorNick, password string) bool {
	storedPassword, present := opersMap[operatorNick]
	if !present {
		return false // no oper has that nickname
	}

	hash := sha256.New()
	hash.Write([]byte(salt + password))
	digest := hash.Sum(nil)
	hexpass := hex.EncodeToString(digest)
	if hexpass == storedPassword {
		return true
	}
	return false
}

// Hashes and salts the password, using the salt that was (hopefully) specified
// in the config file. The output of this function is suitable for inclusion
// into the OPERS file.
func MakeHashedAndSaltedPassword(password string) string {
	hash := sha256.New()
	hash.Write([]byte(salt + password))
	digest := hash.Sum(nil)
	hexpass := hex.EncodeToString(digest)
	return hexpass
}
