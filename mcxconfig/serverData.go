package mcxconfig

import (
	"sync"
	)

var (
	serverData		serverDataStruct	= serverDataStruct{}
	serverDataMutex	sync.RWMutex		= sync.RWMutex{}
	)

// Server structure, containing fairly static data.
// Code outside the mcxconfig package should NEVER update these varaibles.
type serverDataStruct struct {
	FullName        string          // The domain name of the server. (Fake domain names work.)
	ShortName       string          // The name of the server, such as "Bob", "darkstar", "blumenkraft", etc
	Software        string          // The software the server is claiming to be composed of.
	Version         string          // The version of said software.
	Hostcloak       string          // The reported hostname of all clients (will and should always be faked).
	Quote           string          // Some nice quote that is occationally displayed by the server.
	DefaultRealName string          // Instead of reporting the real name of users, this name is used.
	MaxNickLen      int             // The maximum length of nicknames for users.
	ListenOn        []string        // Addresses for the server to listen at (most probably IPv4/6-addresses). If it only contains a single "" then it listens on ALL available addresses.
	IRCPort         int             // Port for the MCX IRC server to listen at (on all addresses specified in ListenOn)
	StartWebserver  bool            // True if the web server should be running
	WebPort         int             // Port for the MCX Web server to listen at (on all addresses specified in ListenOn)
	UseI2P          bool            // Use I2P?
	SAMBridge       string          // IPv4:port to I2Ps SAMv3 bridge
	DataDir         string          // directory to store generic data into
}

// Returns the servers full name (domain name, or similiar)
func GetFullName() string {
	serverDataMutex.RLock()
	v := serverData.FullName
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers full name (the "domain name" of the server). Doing this mid-flight will VERY LIKELY cause serious havoc. Returns previous name.
func SetFullName(newName string) string {
	serverDataMutex.Lock()
	v := serverData.FullName
	serverData.FullName = newName
	serverDataMutex.Unlock()
	return v
}

// Returns the servers short name
func GetShortName() string {
	serverDataMutex.RLock()
	v := serverData.ShortName
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers full name. Doing this mid-flight will MAYBE NOT cause serious havoc. Returns previous name.
func SetShortName(newName string) string {
	serverDataMutex.Lock()
	v := serverData.ShortName
	serverData.ShortName = newName
	serverDataMutex.Unlock()
	return v
}

// Returns the name of the software the server is running ("mcxhub", unless you changed it)
func GetSoftware() string {
	serverDataMutex.RLock()
	v := serverData.Software
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers reported software. Doing this mid-flight will VERY LIKELY cause serious havoc. Returns previous value.
func SetSoftware(newName string) string {
	serverDataMutex.Lock()
	v := serverData.Software
	serverData.Software = newName
	serverDataMutex.Unlock()
	return v
}

// Returns the servers version.
func GetVersion() string {
	serverDataMutex.RLock()
	v := serverData.Version
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers version. Doing this mid-flight will VERY LIKELY cause serious havoc. Returns previous value.
func SetVersion(newName string) string {
	serverDataMutex.Lock()
	v := serverData.Version
	serverData.Version = newName
	serverDataMutex.Unlock()
	return v
}

// Returns the hostcloak that is used to conceal the real/fake host/ip that users have
func GetHostcloak() string {
	serverDataMutex.RLock()
	v := serverData.Hostcloak
	serverDataMutex.RUnlock()
	return v
}

// Sets the hostcloak. Doing this mid-flight will MAYBE cause serious havoc. Returns previous value.
func SetHostcloak(newName string) string {
	serverDataMutex.Lock()
	v := serverData.Hostcloak
	serverData.Hostcloak = newName
	serverDataMutex.Unlock()
	return v
}

// Returns the quote that your server occationally, at random times, output.
func GetQuote() string {
	serverDataMutex.RLock()
	v := serverData.Quote
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers quote. Doing this mid-flight is PERFECTLY SAFE. Returns previous value.
func SetQuote(newQuote string) string {
	serverDataMutex.Lock()
	v := serverData.Quote
	serverData.Quote = newQuote
	serverDataMutex.Unlock()
	return v
}

// Returns the "real name" that is reported for users. In MCXHUB, this name is of course hidden, and replaced with a dummy.
func GetDefaultRealName() string {
	serverDataMutex.RLock()
	v := serverData.DefaultRealName
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers default real name; The name that replaces every users "real name". The Real Name may contain spaces. Returns previous value.
func SetDefaultRealName(newRealName string) string {
	serverDataMutex.Lock()
	v := serverData.DefaultRealName
	serverData.DefaultRealName = newRealName
	serverDataMutex.Unlock()
	return v
}

// Returns the maximum nickname length
func GetMaxNickLen() int {
	serverDataMutex.RLock()
	v := serverData.MaxNickLen
	serverDataMutex.RUnlock()
	return v
}

// Sets the servers maximum nickname length. Doing this mid-flight is slightly weird. Returns previous value.
func SetMaxNickLen(newLength int) int {
	serverDataMutex.Lock()
	v := serverData.MaxNickLen
	serverData.MaxNickLen = newLength
	serverDataMutex.Unlock()
	return v
}

// Returns the addresses (IPv4 and IPv6) that MCXHUB is currently (attempting to) listening on.
func GetListenOn() []string {
	serverDataMutex.RLock()
	v := serverData.ListenOn
	serverDataMutex.RUnlock()
	return v
}

// Sets what addresses (IPv4 and IPv6) the server will be listening on. If you change this, you will also need to notify *all* services to switch to these addresses.
func SetListenOn(newAddressList []string) []string {
	serverDataMutex.Lock()
	v := serverData.ListenOn
	serverData.ListenOn = newAddressList
	serverDataMutex.Unlock()
	return v
}

// Returns the port the IRC server is listening on
func GetIRCPort() int {
	serverDataMutex.RLock()
	v := serverData.IRCPort
	serverDataMutex.RUnlock()
	return v
}

// Sets the port the IRC server will be listening on. If you change this, you will also need to notify the IRC server, to start using this port.
func SetIRCPort(newPort int) int {
	serverDataMutex.Lock()
	v := serverData.IRCPort
	serverData.IRCPort = newPort
	serverDataMutex.Unlock()
	return v
}

// Returns true if MCXHUB is configured to start the web server
func GetStartWebserver() bool {
	serverDataMutex.RLock()
	v := serverData.StartWebserver
	serverDataMutex.RUnlock()
	return v
}

// Set this to true, if you want the webserver to be available as a service. You also need to notify the webserver about this change.
func SetStartWebserver(b bool) bool {
	serverDataMutex.Lock()
	v := serverData.StartWebserver
	serverData.StartWebserver = b
	serverDataMutex.Unlock()
	return v
}

// Returns the port the Webserver is listening on
func GetWebPort() int {
	serverDataMutex.RLock()
	v := serverData.WebPort
	serverDataMutex.RUnlock()
	return v
}

// Sets the port the webserver is supposed to be listening at. Returns previous value. You also need to notify the webserver to start using the new value.
func SetWebPort(newPort int) int {
	serverDataMutex.Lock()
	v := serverData.WebPort
	serverData.WebPort = newPort
	serverDataMutex.Unlock()
	return v
}

// Returns true if I2P should be used, otherwise false, derp.
func GetUseI2P() bool {
	serverDataMutex.RLock()
	v := serverData.UseI2P
	serverDataMutex.RUnlock()
	return v
}

// Set this to true, if you want to use I2P. All other I2P-settings will be ignored if set to false. Returns previous value.
func SetUseI2P(b bool) bool {
	serverDataMutex.Lock()
	v := serverData.UseI2P
	serverData.UseI2P = b
	serverDataMutex.Unlock()
	return v
}

func GetSAMBridge() string {
	serverDataMutex.RLock()
	v := serverData.SAMBridge
	serverDataMutex.RUnlock()
	return v
}

// Set the address (IPv4:port) of the I2P SAM bridge. Returns the previous value.
func SetSAMBridge(samBridge string) string {
	serverDataMutex.Lock()
	v := serverData.SAMBridge
	serverData.SAMBridge = samBridge
	serverDataMutex.Unlock()
	return v
}

// Returns the generic data folder, used by everything that needs to remember things and settings
func GetDataDir() string {
	serverDataMutex.RLock()
	v := serverData.DataDir
	serverDataMutex.RUnlock()
	return v
}

// Set the generic data folder. Returns the previous value. It probably is very, very, very stupid to change this while running.
func SetDataDir(dataDir string) string {
	serverDataMutex.Lock()
	v := serverData.DataDir
	serverData.DataDir = dataDir
	serverDataMutex.Unlock()
	return v
}


