package main

import (
	"code.google.com/p/go.net/websocket"
	"bufio"
	"log"
	"fmt"
	)

func main() {
	origin := "http://localhost/"
	url := "ws://localhost:6502/api/ircsocket"
	ws, err := websocket.Dial(url, "", origin)
	if err != nil {
		log.Fatal(err)
	}
	if _, err := ws.Write([]byte("NICK testws\r\n")); err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(ws)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		fmt.Printf("%s\n", scanner.Text())
	}
}
