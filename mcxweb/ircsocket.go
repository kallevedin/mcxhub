package mcxweb

import (
	"golang.org/x/net/websocket"
	"bitbucket.org/kallevedin/mcxhub/mcxircd"
	"bufio"
	"net"
	)


// Takes a websocket and attaches it to the IRC server. Does not reformat the
// datastream. Useful if you want to connect *anything*.
// Blocks until the (servers internal representation of the) IRCUser dies.
func RawIRCWebSocket(ws *websocket.Conn) {
    ircUser, ok := mcxircd.Handshake(ws)
    if !ok {
    	ws.Write([]byte("ERROR: Websocket could not be attached to IRC server.\n"))
    	return
    }
    // just wait for it to die, and then die
	ircUser.WaitForDeath.Wait()
}

// HTTP/WebSocket suitable for connecting javascript clients, and whatnot.
// 
// Takes a websocket and attaches it to the IRC server, such that whatever you
// write to the websocket is handled just like as it was an IRC connection. It
// also does some rewriting of the TCP stream, so that each websocket frame 
// replied back to you will consist of only one IRC command/response message, 
// and also removes trailing \r\n (weird IRC-syntax newlines).
// All commands you send to the socket NEED to end with newlines (either \n or
// \r\n), and you are allowed to send frames that contain multiple lines.
// See also RawIRCWebSocket().
// Blocks until connection disconnect.
func IRCWebSocket(ws *websocket.Conn) {
	// Client <-> WebSocket <-> IRCWebSocket <-> Pipe <-> IRCServer

	// make a pipe and give one end to the IRCServer
	myConn, ircServerConn := net.Pipe()
	
	// attach one side of the pipe to the IRC server
    go mcxircd.Handshake(ircServerConn)
		
	// and split into two goroutines, one for each direction of the datastream
	go wsuToServer(ws, myConn)
	serverToWSU(ws, myConn)
}

// WebSocketUser to Server. (See IRCWebSocket)
func wsuToServer(fromUser, toServer net.Conn) {
	scanner := bufio.NewScanner(fromUser)
	scanner.Split(bufio.ScanLines)
	for {
		if !scanner.Scan() {
			fromUser.Close()
			toServer.Close()
			return // disconnect on any error
		}
		if _,e := toServer.Write([]byte(scanner.Text()+"\r\n")); e!=nil {
			fromUser.Close()
			toServer.Close()
			return // disconnect on any error
		}
	}
}

// Server to WebSocketUser. (See IRCWebSocket)
func serverToWSU(toUser, fromServer net.Conn) {
	scanner := bufio.NewScanner(fromServer)
	scanner.Split(bufio.ScanLines)
	for {
		if !scanner.Scan() {
			toUser.Close()
			fromServer.Close()
			return // disconnect on any error
		}
		if _,e := toUser.Write([]byte(scanner.Text())); e!=nil {
			toUser.Close()
			fromServer.Close()
			return // disconnect on any error
		}
	}
}

