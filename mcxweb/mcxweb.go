// Webserver for mcxhub
package mcxweb

import (
	"golang.org/x/net/websocket"
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)


var (
	//webTemplates *template.Template
	webRoot		string
)

// Checks if the web directory exist and is properly populated. Returns true if everything seems OK.
func checkWebDir(webRoot string) (string, bool) {
	if file, err := os.Open(webRoot); err!=nil {
		s := fmt.Sprintf("Could not open %s\nError: %s\nYou need a web directory. Copy mcxhub/mcxweb/web from the source files?\n", webRoot, err.Error())
		return s, false
	} else {
		if fileInfo, err := file.Readdir(0); err!=nil {
			s := fmt.Sprintf("Could not read the content of your web directory (%s)\nError: %s\n", webRoot, err.Error())
			return s, false
		} else {
			hasIndex := false
			hasFS := false
			s := ""
			for _,fi := range fileInfo {
				if fi.Name() == "index.html" && !fi.IsDir() { 
					hasIndex = true
				}
				if fi.Name() == "fs" && fi.IsDir() {
					hasFS = true
				}
			}
			if !hasIndex {
				s += fmt.Sprintf("No index.html found in your web directory root (.../web/index.html)\n")
			}
			if !hasFS {
				s += fmt.Sprintf("You need a directory named \"fs\" in your web directory (.../web/fs/)\n")
			}
			return s, hasIndex && hasFS
		}
	}
	panic("ERROR in mcxweb.checkWebDir()\n")
}

// Starts the mcxhub web server. Assumes the presence of an initialized mcxconfig. Does not return, unless error or shutdown occured.
func StartWebServer() {
	wd, e := os.Getwd()
	if e != nil {
		log.Printf("FATAL: Could not determine current working directory.\nError: %s\n", e.Error())
		os.Exit(1)
	}
	webRoot = wd + "/web/"
	if errMsg, ok := checkWebDir(webRoot); !ok {
		log.Printf("MCXWEB: Failed to start because:\n%s", errMsg)
		return
	}
	log.Printf("MCXWEB: Using web root directory: %s\n", webRoot)

	http.HandleFunc("/", defaultHandler)
	http.Handle("/fs/", http.StripPrefix("/fs/", http.FileServer(http.Dir(wd+"/web/fs/"))))
	http.Handle("/api/rawircsocket", websocket.Handler(RawIRCWebSocket))
	http.Handle("/api/ircsocket", websocket.Handler(IRCWebSocket))

	for _,addr := range mcxconfig.GetListenOn() {
		go func(thisAddr string, thisPort int){
			log.Printf("MCXWEB: Webserver listening on %s:%d", thisAddr, thisPort)
			err := http.ListenAndServe( thisAddr + ":" + strconv.Itoa(thisPort), nil) // blocks forever if successful
			if err!=nil {
				log.Printf("MCXWEB: Could not listen on \"%s:%d\" because %s\n", thisAddr, thisPort, err.Error())
			}
		}(addr, mcxconfig.GetWebPort())
	}
}
