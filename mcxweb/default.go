package mcxweb

import (
	"net/http"
	"html/template"
	"log"
	"os"
	)

const (
	FRONTPAGE = "index.html"
	DefaultErrorMessage = "ERROR\n\n\nS A T O R\nA R E P O\nT E N E T\nO P E R A\nR O T A S"
	)

// Default handler for incomming HTTP requests.
func defaultHandler(w http.ResponseWriter, r *http.Request) {
	// check if it is the root, if so, reply with the index.html file
	// try to open the file exactly as it is named in the URL
	// try to open the URL with .html attached to it
	// return 404 Not Found

	if "/" == r.URL.Path {
		defaultServePage(w, r, FRONTPAGE)
		return
	}

	vFileName := r.URL.Path[1:len(r.URL.Path)]

	if fi,err := os.Lstat(webRoot + vFileName); err==nil && fi.Mode().IsRegular() {
		defaultServePage(w, r, vFileName)
	} else if fi,err := os.Lstat(webRoot + vFileName + ".html"); err==nil && fi.Mode().IsRegular() {
		defaultServePage(w, r, vFileName + ".html")
	} else {
		http.NotFound(w, r)
	}
}

// Loads a file, parse its content, make a web page out of it, and drop it at the client. Returns true on success.
func defaultServePage(w http.ResponseWriter, r *http.Request, filename string) bool {
 	if templ,err := template.ParseFiles(webRoot + filename); err!=nil {
		log.Printf("MCXWEB: Could not parse %s, because: %s", filename, err.Error())
		http.Error(w, DefaultErrorMessage, http.StatusInternalServerError)
		return false
	} else {
		WebVariables := MakeWebVariables(r)
		if err := templ.ExecuteTemplate(w, filename, WebVariables); err!=nil {
			log.Printf("MCXWEB: Could not execute correctly parsed file %s because: %s", filename, err.Error())
			http.Error(w, DefaultErrorMessage, http.StatusInternalServerError)
			return false
		}
	}
	return true
}
