package mcxweb

import (
	"net/http"
	"strings"
	"bitbucket.org/kallevedin/mcxhub/mcxconfig"
	)

type WebVariables struct {
	FullName      string
	ShortName     string
	Software      string
	Version       string
	Quote         string
	IRCPort       int
	WebPort       int
	Host          string
}

// extracts the hostname of the server that the client connected to
func extractHost(r *http.Request) string {
	hostPort := strings.SplitN(r.Host, ":", 2)
	if len(hostPort) != 2 {
		return r.Host
	}
	return hostPort[0]
}

// Extracts and creates a WebVariables datastruct, to be used in http templates
func MakeWebVariables(r *http.Request) WebVariables {
	return WebVariables{	FullName: mcxconfig.GetFullName(),
							ShortName: mcxconfig.GetShortName(),
							Software: mcxconfig.GetSoftware(),
							Version: mcxconfig.GetVersion(),
							Quote: mcxconfig.GetQuote(),
							IRCPort: mcxconfig.GetIRCPort(),
							Host: extractHost(r),
							}
}



